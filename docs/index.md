# Accueil

Bienvenue sur le site de cours et activités de **terminale** de la spécialité INFO.

<gif-player src="https://media.giphy.com/media/TN0GTccRi7Ixa/giphy.gif" speed="0.5" play></gif-player>

<!-- METTRE ICI LE DIAPORAMA DE PRÉSENTATION DE LA SPÉ NSI 

Pour voir le diaporama suivant :

=== "sur ordinateur"
    Cliquer dessus l'image puis appuyer sur la touche ++f++ pour l'afficher en mode plein écran (f pour fullscreen), appuyer sur la touche ++escape++ pour quitter le mode plein écran.

    Faire défiler les pages avec les flêches de direction du clavier :
    ++up++
    ++down++
    ++left++
    ++right++ ou en cliquant sur les flêches en bas à droite du diaporama :
    ![flêches du diaporama](./data/diapo_fleches.png){width="50"}
    
    Appuyer sur la touche ++m++ pour afficher le menu sur la gauche du diaporama.

    La touche ++home++ permet de revenir rapidement au début du diaporama.

    La touche ++o++ permet de consulter une vue d'ensemble (o pour overview) du diaporama (où la navigation se fait aussi à l'aide des touches flêchées).
    
    <style>
    .diaporama {
    position: relative;
    border: 1px solid black;
    width: 50%;
    height: 40vh;
    /* Les 2 lignes suivantes pour centrer l'iframe : */
    margin: auto;
    display: block; }
    </style>

    <iframe class="diaporama" src="./intro/diaporama.html"> </iframe>

=== "sur smartphone ou tablette"
    Laisser le doigt appuyé sur l'image ci-dessous et choisir `Ouvrir le lien dans un nouvel onglet` puis se servir des fonctionnalités de l'écran tactile pour naviguer dans le diaporama : en faisant glisser le doigt ou en cliquant sur les flêches en bas à droite du diaporama :
    ![flêches du diaporama](./data/diapo_fleches.png){width="50"}

    A la fin du diaporama, fermer l'onglet pour afficher de nouveau le site spé info.

    Image cliquable : 

    <figure markdown> 
    [![capture de la 1ère page du diaporama](./data/capt.png){ width="400" }](./intro/diaporama.html "toucher ou cliquer pour ouvrir le diaporama")
    <figcaption>Image cliquable</figcaption>
    </figure>

-->




<!-- [![capture de la 1ère page du diaporama](./data/capt.png)](./intro/diaporama.html "toucher ou cliquer pour ouvrir le diaporama")

<figure> 
    <img src="./data/capt.png" width="300"> 
    <figcaption>Image cliquable</figcaption>
</figure> 

-->

<!--     [cliquer pour ouvrir le diaporama](./intro/diaporama.html)
    [capture de la 1ère page du diaporama](./intro/diaporama.html "toucher pour ouvrir") -->

<!-- <style>
.diaporama {
 position: relative;
 border: 1px solid black;
 width: 50%;
 height: 30vh;
 /* Les 2 lignes suivantes pour centrer l'iframe : */
 margin: auto;
 display: block; }
</style>

<iframe class="diaporama" src="./intro/diaporama.html"> </iframe> -->

<!-- [lien vers le diaporama](./intro/diaporama.html "cliquer droit pour ouvrir le lien du diaporama dans un nouvel onglet du navigateur")
 -->
