# Corrigé sujet 0b 2023

## EXERCICE 1

1.a. Les commandes permettant de se positionner dans `timbres` depuis `fiches` sont : commande **1** et commande **5**

1.b. Pour accéder au répertoire `timbres` à partir la racine, on peut écrire : `cd /home/document/collections/timbres`

2.a. On applique la formule $C = \dfrac{10^8}{d}$ avec $d = 100$ Mbit/s c'est-à-dire le coût d'une liaison FastEthernet :
$C = \dfrac{10^8}{100\times 10^6} = 1$. Donc le coût d'une liaison FastEthernet est de 1.

2.b. On a reproduit ci-dessous le schéma du réseau, en faisant figurer le coût des liaisons (le coût d'une liaison FFTH est $\dfrac{10^8}{10 \times 10^9}=0,01$) :  

![reseau](data/23-NSIZERO-B1.png){width=600}  

Selon le protocole OSPF (minimisation des coûts), le chemin suivi sera donc : **A -> B -> C -> E -> F -> G** pour un coût total de **1,04**.

3.
Les **descripteurs** de ce fichier sont :  
    - **nom_timbre** avec pour valeurs `Gustave Eiffel`, `Marianne` et `Alan Turing`,  
    - **annee_fabrication** avec pour valeurs `1950`, `1989` et `2021`,  
    - **nom_collectionneur** avec pour valeurs `Dupont`, `Durand` et ̀`Dupont`.

4.a. La clé primaire d'une relation est un attribut (ou un ensemble d'attributs) permettant d'identifier de façon unique chaque enregistrement.

4.b. L'attribut `nom` ne peut pas servir de clé primaire car il peut ne pas être unique dans la relation. En effet, dans l'exemple proposé plusieurs timbres ont pour nom `Gustave Eiffel`.

4.c. Pour la même raison, l'attribut `annee_fabrication` ne peut pas servir de clé primaire non plus. Dans l'exemple proposé 2 timbres ont pour année de fabrication 1989.

4.d. On peut ajouter un attribut `id_timbre` qui sera différent pour chaque enregistrement (par exemple en l'incrémentant de 1 à chaque ajout d'un timbre)

5.a. Cette requête modifie l'attribut `ref_licence` en `Ythpswz` pour les enregistrements dont l'attribut `nom` est `Dupond`. Après cette requête la relation devient (en italique, les valeurs modifiées):

|ref_licence|nom       |prenom         |annee_naissance|nbre_timbres|
|-----------|----------|---------------|---------------|------------|
|Hqdfapo|Dupuis|Daniel|1953|53|
|*Ythpswz*|Dupond|Jean-Pierre|1961|157|
|Qdfqnay|Zaouï|Jamel|1973|200|
|Aerazri|Pierre|Jean|1967|130|
|*Ythpswz*|Dupond|Alexandra|1960|61|

5.b. L'attribut `ref_licence` ne peut plus être une clé primaire puisqu'il est n'est plus unique (la valeur `Ythpswz`  apparaît pour 2 enregistrements) aux lignes 2 et 5.

6.
```sql
SELECT nom, prenom, nbre_timbres 
FROM collectionneurs
WHERE annee_naissance >= 1963;
```

## EXERCICE 2

1.a. Une fonction récursive est une fonction qui possède, dans son code, un appel à elle-même.  

1.b. Une fois l'affichage de ``0`` effectué, il y a un appel récursif ``compte_rebours(0 - 1)``.
Lors de cet appel récursif, ``n`` vaut ``-1``, on ne rentre donc pas dans la structure conditionnelle (``if``).
La pile d'appel récursif se vide sans qu'il y ait d'autres instructions effectuées.
Ainsi le programme s'arrête après avoir affiché ``0`` et vidé la pile d'appels récursifs.

2.

```python
def fact(n):
    """ Renvoie le produit des entiers strictement positifs
        et inférieurs ou égaux à n.
    """
    if n == 0:
        return 1
    else:
        return n * fact(n - 1)
```

3.a.

```python
def somme_entiers_rec(n):
    """ Renvoie, de manière récursive,
    la somme des entiers de 0 à l'entier naturel n.
    """
    if n == 0:
        return 0
    else:
        print(n)  # pour vérification
        return n + somme_entiers(n - 1)
```

```python
>>> res = somme_entiers_rec(3)
3
2
1
>>>
```

**Explications** :

- L'appel ``somme_entiers_rec(3)`` affiche 3 puis appelle ``somme_entiers_rec(2)``
- L'appel ``somme_entiers_rec(2)`` affiche 2 puis appelle ``somme_entiers_rec(1)``
- L'appel ``somme_entiers_rec(1)`` affiche 3 puis appelle ``somme_entiers_rec(0)``
- L'appel ``somme_entiers_rec(0)`` n'affiche rien.

3.b. La valeur 6 sera affectée à la variable `res` ($res = 0 + 1 + 2 + 3$)

4.

```python
def somme_entiers(n):
    """ Renvoie, de façon itérative (boucle for),
    la somme des entiers de 0 à l'entier naturel n compris.
    """
    somme = 0
    for entier in range(1, n+1):
        somme = somme + entier
    return somme
```

```python
def somme_entiers(n):
    """ Renvoie, de façon itérative (boucle while),
    la somme des entiers de 0 à l'entier naturel n compris.
    """
    somme = 0
    while n > 0:
        somme = somme + n
        n = n - 1 
    return somme
```

<!-- ## EXERCICE 3

1.a. Un exemple d'attribut de la classe `ArbreBinaire` est `valeur`.
     Un exemple de méthode est `insert_gauche()`.

1.b. Après l'exécution de ce code, `a` aura le valeur 15 et `c` la valeur 6.

1. 
![arbre1](data/23-arbre1.png)

1. La valeur `13` figure dans le sous-arbre gauche du noeud `12` qui ne doit contenir que des valeurs *plus petites* que 12 si l'arbre était binaire. La valeur `11` figure dans le sous-arbre droit du noeud `12` qui ne doit contenir que des valeurs *plus grandes* que 12 si l'arbre était binaire.
On obtient un arbre binaire de recherche en permutant les positions de `13` et `11`.

![arbre2](data/23-arbre2.png)

4. La liste renvoyée sera : `[1, 6, 10, 15, 16, 18, 25]`. On rappelle que dans un parcours infixe, on parcourt d'abord le sous-arbre gauche, puis la racine puis le sous-arbre droit. Dans le cas d'un arbre binaire de recherche, ce parcours permet d'obtenir les valeurs dans l'ordre croissant.  -->


```mermaid
    graph TD
    S14["14"] --- V1[" "]
    S14 --> S15["15"]
    S15 --- V2[" "]
    S15 --> S19["19"]
    style V1 fill:#FFFFFF, stroke:#FFFFFF
    linkStyle 0 stroke:#FFFFFF,stroke-width:0px
    style V2 fill:#FFFFFF, stroke:#FFFFFF
    linkStyle 2 stroke:#FFFFFF,stroke-width:0px
```