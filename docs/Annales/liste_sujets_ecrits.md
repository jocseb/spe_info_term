# Sujets écrits de la spé info du Baccalauréat

## 2020

- [Sujet 0](data/2020/sujet_0.pdf){:target="_blank"}

## 2023

- [Sujet 0 - version A](data/2023/23-NSIZERO-A.pdf){:target="_blank"}
- [Sujet 0 - version B](data/2023/23-NSIZERO-B.pdf){:target="_blank"}
