---
title: "Gestion des processus et des ressources"
author: []
date: "02 avril 2022"
subject: "Gestion des processus"
keywords: [SE, processus]
lang: "fr"
header-left: T^ale^ NSI
header-center: Gestion des processus et des ressources
header-right: Cours
footer-left: 
footer-center: "Thème: Architectures matérielles, système d'exploitation et réseaux"
footer-right: "Page \\thepage"
...

# Gestion des processus et des ressources par le système d'exploitation

Les systèmes d'exploitation (SE) multitâches sont la norme. Il sont **multitâches** car les programmes s'exécutent "en même temps". Pourtant, l'ordinateur ne dispose que d'un nombre limité de processeurs (coeurs, core en anglais, de 1 à 16 pour les ordinateurs personnels actuels). Or un programme n'est qu'une suite d'instructions en langage machine, ces dernières étant exécutées 1 par 1 par le processeur. 

Alors, comment le processeur peut-il donc exécuter "en même temps" les instructions de 2 programmes ?

Grâce à ce qu'on appelle la **programmation concurrente**. Les programmes d'un SE sont alors exécuter de manière concurrente : leurs exécutions sont entrelacées.

L'exécution d'un programme s'appelle un **processus**. Dans la suite, on verra la gestion des processus par le SE.

## 1. Notion de processus

### 1.1 Définition d'un processus

Lorsqu'un programme est exécuté sur un ordinateur, celui-ci va créer un (ou plusieurs) **processus**.

On dit que ce processus est une **instance d'exécution** de ce programme.

Un processus est caractérisé par :

- l'ensemble des instructions qu'il va devoir accomplir (écrites dans le fichier exécutable obtenu par la compilation du code-source du programme)
- les ressources que le programme va mobiliser (fichier en ouverture, carte son, ...)
- l'état des registres du processeur.

\newpage
### 1.2 Observation des processus sous Linux

### 1.2.1 La commande ```ps```

Dans un terminal, la commande ```ps``` va permettre d'afficher la liste des processus actifs.

> Plus précisément, nous allons utiliser la commande ```ps -lu nom_utilisateur```. L'option ```l``` (long) permet d'afficher un grand nombre de renseignements et l'option ```u``` permet de spécifier l'utilisateur propriétaire des processus.

![Commande ps -lu nom_utilisateur](data/terminal1_debut.png){ width=70% }

On retrouve notamment dans ce tableau les colonnes :

- `UID` (User IDentifier) : l'identifiant de l'utilisateur, ici l'utilisateur slinux a un UID égal à 1000 ;

- ```CMD```  (CoMmanD) : le nom de la commande qui a créé le processus. On peut y retrouver par ordre chronologique le nom de tous les programmes actifs. Certains sont ceux que l'utilisateur a ouvert volontairement (navigateur, environnement de développement comme Visual Studio Code, ...) mais on y trouve surtout tous les programmes nécessaires au bon fonctionnement du système d'exploitation. Le dernier processus en bas de la liste est forcément ```ps```, puisque l'utilisateur vient de l'appeller :

![ps -lu nom_utilisateur](data/terminal1_fin.png){ width=70% }

- ```PID``` (Process IDentifier) : IDentifiant du Processus, c'est le numéro unique d'identification du processus, affecté chronologiquement par le système d'exploitation.

- ```PPID``` (Parent PID) : PID du processus parent, certains processus vont *eux-mêmes* lancer plusieurs processus-fils, qui porteront le même nom. C'est ainsi qu'on peut retrouver de multiples processus s'appelant ```code``` (voir ci-dessous) :

![ps -lu nom_utilisateur](data/terminal1_code.png){ width=70% }

Ici, l'instance «principale» de Visual Studio Code (```PID``` = 6128 pour `code`) a généré plusieurs instances de ```PID``` différents (les `enfants` de 6128), dont le ```PPID``` vaut 6128.


> On peut aussi utiliser la commande ```ps -ef``` qui permet d'afficher plus d'informations que la précédente :

![Commande ps -ef](data/terminal_ef_debut.png){ width=70% }

![Commande ps -ef](data/terminal_ef_fin.png){ width=70% }


### 1.2.2 La commande ```pstree```

À noter que la commande ```pstree``` permet d'afficher les processus sous forme d'arborescence :

![Commande pstree](data/pstree_code.png){ width=50% }

### 1.2.3 La commande ```top```

La commande ```top``` permet de connaître en temps réel la liste des processus, classés par ordre décroissant de consommation de CPU. 

> On ferme ```top``` en tapant sur la touche `q` ou par la combinaison de touches ```Ctrl``` + `C`.

![Affichage du résultat de la commande top](data/top.png){ width=50% }

Si on repère alors un processus qui consomme beaucoup trop de ressources, on peut utiliser...

### 1.2.4 La commande ```kill```
La commande ```kill``` permet de fermer un processus, en donnant son ```PID```  en argument.

Exemple : `kill` `6128` tuera `code` (voir la figure 3), c'est-à-dire le processus `6128` et tous ses enfants.

## 2. Ordonnancement de processus

### 2.1 Expérience : les processus fonctionnent ~~simultanément~~ à tour de rôle.  
Un ordinateur donne l'illusion de réaliser plusieurs tâches simultanément. Hormis pour les processeurs fonctionnant avec plusieurs cœurs, il n'en est rien.

Comme nous l'avons vu, ces processus sont lancés séquentiellement par le système d'exploitation, et sont ensuite en apparence tous «actifs en même temps» (les guillemets sont importants) : on parle de **programmation concurrente**.

Revenons sur l'expression «actifs en même temps», car elle véhicule une fausse idée : ces processus sont bien vivants dans un même laps de temps, mais ils s'exécutent **LES UNS APRÈS LES AUTRES**, le processeur ne pouvant en traiter qu'**un seul à la fois**.

Un cadencement extrêmement rapide et efficace donne l'*apparence* d'une (fausse) simultanéité. Nous allons la mettre en évidence :

Considérons les fichiers ```progA.py``` et ```progB.py``` ci-dessous :

```python
# progA.py
from time import sleep

for i in range(10):
    print("programme A en cours, itération", i)
    sleep(0.02)  
```
<!--
Résultat:

![image](data/progA.png){ width=20% } -->

```python
# progB.py
from time import sleep
sleep(0.01)
for i in range(10):
    print("programme B en cours, itération", i)
    sleep(0.02)  
```

Si on exécute ces 2 programmes l'un après l'autre les résultats affichés dans la console Python sont les suivants:

<!-- ![image](data/progB.png){ width=20% } -->

| progA   | progB |
| --------------------------- | ----------------------------------------- |
|   ![image](data/progA.png){ width=40% }   |  ![image](data/progB.png){ width=40% }       |

Si on tente d'exécuter ces 2 programmes, non plus l'un après l'autre, mais en même temps, et en tenant compte que le programme ```progB.py``` est légèrement retardé au démarrage par le ```sleep(0.01)```, il devrait donc en résulter un entrelacement entre les phrases ```programme A en cours``` et ```programme B en cours```.  

L'exécution «d'apparence simultanée» de ces deux programmes peut se faire dans un terminal via la commande ```python3 progA.py & python3 progB.py```.

Il en résulte ceci :

![```python3 progA.py & python3 progB.py```](data/progA_progB.png){ width=30% }

Nous retrouvons bien l'alternance prévue à la lecture du code.  
Tout se passe donc comme si les 2 processus avaient été lancés et traités simultanément.

Réduisons maintenant les temporisations (en passant du centième de seconde au dixième de milliseconde) et renommons les fichiers `progC` et `progD`: 

```python
# progC.py
from time import sleep

for i in range(10):
    print("programme C en cours, itération", i)
    sleep(0.0002) 
```

```python
# progD.py
from time import sleep
sleep(0.001)
for i in range(10):
    print("programme D en cours, itération", i)
    sleep(0.0002)
```

Dans un terminal, on exécute la commande ```python3 progC.py & python3 progD.py```. 

Il en résulte maintenant ceci :

<!-- ![image](data/progC_progD_v1.png){ width=20% } -->

<!-- ![image](data/progC_progD_v2.png){ width=20% } -->

| Exécution n°1    | Exécution n°2 |
| --------------------------- | ----------------------------------------- |
| ```python3 progC.py & python3 progD.py```    | ```python3 progC.py & python3 progD.py``` |
| ![image](data/progC_progD_v1.png){ width=30% } | ![image](data/progC_progD_v2.png){ width=30% } |

L'alternance prévue n'est plus respectée et les résultats deviennent non-reproductibles, on remarque que 2 commandes identiques ne donnent pas le même résultat.

Si la gestion des processus était réellement simultanée, même en considérant des ralentissements du processeur par des sollicitations extérieures, chaque processus serait ralenti de la même manière : l'**entrelacement** des phrases serait toujours le même.

En réalité, le processeur passe son temps à alterner entre les divers processus qu'il a à gérer, et les met en attente quand il ne peut pas s'occuper d'eux. Il obéit pour cela aux instructions de son **ordonnanceur**.

## 2.2 L'ordonnancement des processus

### 2.2.1 Différents types d'ordonnancement

Si on vous donne 4 tâches A, B, C et D à accomplir, vous pouvez décider :

- de faire la tâche prioritaire d'abord ;
- de faire la tâche la plus rapide d'abord ;
- de faire la tâche la plus longue d'abord ;
- de les faire dans l'ordre où elles vous ont été données ;
- de faire à tour de rôle chaque tâche pendant un temps fixe jusqu'à ce qu'elles soient toutes terminées;
- ...

Un processeur est confronté aux mêmes choix : 

comment déterminer quel processus doit être traité à quel moment ?

Le schéma ci-dessous (issu de [ce site](https://medium.com/@sheenam.mca17.du/process-scheduling-b86975413079)) présente quelques politiques d'ordonnancement :

![Algorithmes d'ordonnancement](data/schedule.png)

Sous Linux, l'ordonnancement est effectué par un système hybride où les processus sont exécutés à tour de rôle (on parle de *tourniquet* ou de *Round Robin*) suivant un ordre de priorité dynamique.

> Dans le cas (très fréquent maintenant) d'un processeur *multi-cœurs*, le problème reste identique. Certes, sur 4 cœurs, 4 processus pourront être traités simultanément (une **réelle** simultanéité) mais il reste toujours beaucoup plus de processus à traiter que de cœurs dans le processeur... et un ordonnancement est donc toujours nécessaire.

\newpage
### 2.2.2 Les différents états d'un processus

Selon que l'ordonnanceur aura décidé de le confier ou non au processeur pour son exécution, un processus peut donc se trouver dans 3 états :

- **Prêt** : il attend qu'arrive le moment de son exécution.
- **Élu** : il est en cours d'exécution par le processeur.
- **Bloqué** : pendant son exécution (état **Élu**), le processus réclame une ressource qui n'est pas immédiatement disponible. Son exécution s'interrompt (blocage). Lorsque la ressource sera disponible, le processus repassera par l'état **Prêt** (déblocage) et attendra à nouveau son tour.

![Différents états d'un processus](data/cycle.png)

On peut utiliser la métaphore suivante :

> Sur le bureau d'un professeur, il y a 3 paquets de copies, correspondant aux classes A, B, et C. Ces paquets sont **Prêts** à être corrigés. Si le professeur ramène devant lui le paquet A, celui-ci devient **Élu**, et le professeur peut commencer à le corriger. Pour se changer les idées, il peut interrompre la correction du paquet A (qui va passer à l'état **Bloqué**) et ramener vers lui le paquet C. Il pourra ensuite prendre le paquet B, puis à nouveau le C, puis le A, ainsi de suite jusqu'à ce que tous les paquets soient totalement corrigés. Ces paquets seront alors **Terminés**.
Au cours de cette procédure, le professeur n'a toujours eu devant lui qu'**un seul paquet de copies** (soit A, soit B, soit C).

**Pourquoi l'accès à une ressource peut bloquer un processus ?**

Pendant son exécution, un processus peut avoir besoin d'accéder à une ressource déjà occupée (un fichier déjà ouvert, par exemple) ou être en attente d'une entrée-utilisateur (un ```input()``` dans un code ```Python``` par exemple). Dans ce cas-là, le processeur va passer ce processus à l'état **Bloqué**, pour pouvoir ainsi se consacrer à un autre processus.

Une fois débloqué, le processus va repasser à l'état **Prêt** et rejoindre (par exemple) la *file d'attente* des processus avant d'être à nouveau **Élu** et donc exécuté.

\newpage
## 3. Interblocage

### 3.1 Définition et exemple
Comme nous venons de le voir, un processus peut être dans l'état bloqué dans l'attente de la libération d'une ressource.

Ces ressources (l'accès en écriture à un fichier, à un registre de la mémoire, ...) ne peuvent être données à 2 processus à la fois. Des processus souhaitant accéder à cette ressource sont donc en concurrence sur cette ressource. Un processus peut donc devoir attendre qu'une ressource se libère avant de pouvoir y accéder (et ainsi passer de l'état Bloqué à l'état Prêt).

**Problème :** Et si 2 processus se bloquent mutuellement la ressource dont ils ont besoin ?

**Exemple :** Considérons 2 processus A et B, et 2 ressources R et S. L'action des processus A et B est décrite ci-dessous :

![Interblocage](data/tab_proc.png)

**Déroulement des processus A et B :**

- A et B sont créés et passent à l'état **Prêt**.
- L'ordonnanceur déclare **Élu** le processus A (ou bien B, cela ne change rien).
- L'étape A1 de A est réalisée : la ressource R est donc affectée à A.
- L'ordonnanceur déclare maintenant **Élu** le processus B. A est donc passé à **Bloqué** en attendant que son tour revienne.
- L'étape B1 de B est réalisée : la ressource S est donc affectée à B.
- L'ordonnanceur déclare à nouveau **Élu** le processus A. B est donc passé à **Bloqué** en attendant que son tour revienne.
- L'étape A2 de A est donc enclenchée : problème, il faut pour cela pouvoir accèder à la ressource S, qui n'est pas disponible. L'ordonnanceur va donc passer A à **Bloqué** et va revenir au processus B qui redevient **Élu**.
- L'étape B2 de B est donc enclenchée : problème, il faut pour cela pouvoir accèder à la ressource R, qui n'est pas disponible. L'ordonnanceur va donc passer B à **Bloqué**.

Les deux processus A et B sont donc dans l'état **Bloqué**, chacun en attente de la libération d'une ressource bloquée par l'autre : **ils se bloquent mutuellement**.

Cette situation est appelée **interblocage** ou **deadlock**.

\newpage
### 3.3 Représentation schématique

- les processus seront représentés par des **cercles**, les ressources par des **carrés**.
- Si à l'étape A1 le processus A a demandé et **reçu** la ressource R, la représentation sera :

![le processus A a reçu la ressource R](data/A1.png){ width=30% }

- Si à l'étape A2 le processus A est **en attente** de la ressource S, la représentation sera :

![le processus A est en attente de la ressource](data/A2.png){ width=30% }

Avec ces conventions, la situation précédente peut donc se schématiser par :

![cycle d'interdépendance](data/schema.png){ width=50% }

Ce type de schéma fait apparaître un **cycle d'interdépendance**, qui caractérise ici la situation d'**interblocage** (`deadlock`).

### 3.4 Comment s'en prémunir ?

Il existe trois stratégies pour éviter les interblocages :

- **la prévention** : on oblige le processus à déclarer à l'avance la liste de toutes les ressources auxquelles il va accéder.
- **l'évitement** : on fait en sorte qu'à chaque étape il reste une possibilité d'attribution de ressources qui évite le deadlock.
- **la détection/résolution** : on laisse la situation arriver jusqu'au deadlock, puis un algorithme de résolution détermine quelle ressource libérer pour mettre fin à l'interblocage.

<!--

### 3.5 Le deadlock dans la vie courante

![L'emballage diabolique](data/ciseaux.png)

![Le carrefour maudit](data/stop.png)

![Le chômage éternel](data/job.png)

-->
