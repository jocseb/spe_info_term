## Exercice 4  

**Q1.** Les états possibles d’un processus sont : *prêt*, *élu*, *terminé* et *bloqué*.

**Q1.a.** Expliquer à quoi correspond l’état *élu*.  
**Q1.b.** Proposer un schéma illustrant les passages entre les différents états.

**Correction**  

**Q1a.** Élu signifie que le processus est actuellement en cours d'exécution par le processeur.

**Q1b.**

![image](data/cycle.png)

**Q2.** On suppose que quatre processus C₁, C₂, C₃ et C₄ sont créés sur un ordinateur, et qu’aucun autre processus n’est lancé sur celui-ci, ni préalablement ni pendant l’exécution des quatre processus.
L’ordonnanceur, pour exécuter les différents processus prêts, les place dans une structure de données de type file. Un processus prêt est enfilé et un processus élu est défilé.

**Q2.a.** Parmi les propositions suivantes, recopier celle qui décrit le fonctionnement des entrées/sorties dans une file :  

- i. Premier entré, dernier sorti
- ii. Premier entré, premier sorti
- iii. Dernier entré, premier sorti

**Q2.b.** On suppose que les quatre processus arrivent dans la file et y sont placés dans l’ordre C₁, C₂, C₃ et C₄.

- Les temps d’exécution totaux de C₁, C₂, C₃ et C₄ sont respectivement
100 ms, 150 ms, 80 ms et 60 ms.
- Après 40 ms d’exécution, le processus C₁ demande une opération d’écriture
disque, opération qui dure 200 ms. Pendant cette opération d’écriture, le
processus C₁ passe à l’état bloqué.
- Après 20 ms d’exécution, le processus C₃ demande une opération d’écriture
disque, opération qui dure 10 ms. Pendant cette opération d’écriture, le
processus C₃ passe à l’état bloqué.

Sur la frise chronologique ci-dessous, les états du processus C₂ sont donnés. Compléter la frise avec les états des processus C₁, C₃ et C₄.

![image](data/ex4_frise.png)

**Correction**

**Q2a.** ii. Premier entré, premier sorti (FIFO)

**Q2.b.**

![image](data/COR_exo4_Q2b.png)