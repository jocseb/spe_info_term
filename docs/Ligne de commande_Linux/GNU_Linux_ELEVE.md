---
title: "GNU/LINUX et ligne de commande"
author: []
date: "18 mai 2022"
subject: "GNU / Linux"
keywords: [SE, Linux, ligne de commande]
lang: "fr"
header-left: NSI
header-center: GNU / LINUX ET SON INTERFACE EN LIGNE DE COMMANDE
header-right: Cours
footer-left: 
footer-center: "Thème: Architectures matérielles, système d'exploitation et réseaux"
footer-right: "Page \\thepage"
...


## 1. Système d'exploitation

Le système d’exploitation (Operating System en anglais, on utilisera OS dans la suite) est le logiciel système qui gère l'ordinateur, en jouant un rôle d’intermédiaire entre les programmes, les éléments matériels de l’ordinateur (notamment la mémoire) ainsi que les périphériques d'entrée/sortie. C’est le premier programme exécuté au démarrage de la machine, et c’est le seul qui reste en permanence en exécution.

![Interactions avec le noyau](../images/noyau-kernel.png){width=40%}

La partie de l'OS qui gère la communication avec les éléments matériels s'appelle  **le noyau** (kernel en anglais).

Outre le noyau, l'OS comporte des applications, une interface graphique, la gestion des fichiers, le terminal...


## 2. Interfaces homme-machine sur un OS

### 2.1. Présentation

Un système d'exploitation peut être utilisé avec une **interface graphique** composée de fenêtre et d'icônes :  
 
![interface graphique](../images/bureau.png){width=40%}
![interface graphique](../images/arbo_Documents2.png){width=50%}

mais également avec une **interface en ligne de commande** :  

![interface en ligne de commande](../images/terminal2.png){width=50%}

|Interface graphique |Interface en ligne de commande |
|:-|-:|
|\+ simple | \+ puissante|
|\- change selon les distributions | \+ rapide|
|\- puissante | permet de comprendre|
|               |+ complexe au début|
|Pas toujours disponible | Toujours disponible|
|||  

### 2.2. Historique

Historiquement, les premiers systèmes d’exploitation ne disposaient pas d’interface graphique. D’ailleurs, à cette époque, la souris n’existait même pas. On interagissait donc avec le système essentiellement par échange de texte. 
Les entrées du système se faisaient avec un clavier et les sorties avec du papier imprimé, le tout constituait un télétype (tty).

![Teletype ASR33](../images/teletypeASR33.jpg){width=30%}

Le télétype a été ensuite remplacé par un terminal.
Un terminal est une variété de périphérique réseau placé à l'extrémité d'un nœud. Le terminal est un point d’accès de communication entre l'homme et un ordinateur central ou un réseau d'ordinateurs.
On a donc remplacé la sortie par un écran mais on a gardé pour certains langages l'instruction ``print`` pour demander un affichage dans la console comme en ``Python`` par exemple.
L’écran servait à recevoir les informations fournies par le système sous forme de texte.

Aujourd’hui, même si les interfaces graphiques modernes permettent d’effectuer la plupart des opérations, il est important de connaître quelques-unes de ces lignes de commande qui se saisissent dans un (émulateur de) terminal, qui lance un shell par défaut.

![Emulateur de terminal](../images/terminal2.png){width=40%}

### 2.3. Vocabulaire

**Terminal**  

>L'**émulateur de terminal** (souvent appelé **terminal** tout court) est un programme qui s'exécute dans le gestionnaire de fenêtres. Il sert à exécuter des programmes en mode texte, c'est-à-dire où toute l'interaction avec l'utilisateur se fait avec l'écran (pour afficher du texte) et le clavier (pour entrer du texte). 
Par défaut le terminal exécute un **shell**.
Il existe de nombreux émulateurs de terminaux sous Linux : xterm, rxvt-unicode, LXTerminal, gnome-terminal, konsole, xfce4-terminal (celui de Xubuntu)...

**Shell**  

>Le shell (aussi appelé invite de commande ou interpréteur de commande) est un programme avec une interface en mode texte. Le shell affiche un prompt (représenté par le symbole $) et lit les commandes tapées par l'utilisateur. Ces commandes permettent d'effectuer des tâches courantes sur les fichiers et répertoires, de lancer d'autres programmes, etc. Le shell offre aussi un langage permettant d'effectuer des tâches conditionnelles et/ou répétitives à l'aide de structures de contrôle (conditionnelles, boucles for et while, etc.) Les suites de commandes que l'on souhaite réutiliser peuvent être sauvegardées dans un fichier appelé **script**.
Il existe de nombreux shells sous Linux. Les plus utilisés dans le monde Linux sont ``bash`` et ``zsh``.

**Commande**   

>Une commande est en général un petit programme externe lancé par le shell. Par exemple, la commande `ls` permet d'afficher la liste des fichiers contenus dans le répertoire courant. Il est également possible d'exécuter des programmes plus complexes.

Principalement, les commandes servent à lancer de petits programmes ou à travailler sur les fichiers et les répertoires. Dans les systèmes de type "UNIX" (par exemple GNU/Linux ou macOS), nous avons le système de fichiers est organisé en arborescence :  

![Arborescence dans Xubuntu](../images/arbo_slinux.svg.png){width=50%}

### 2.4. Ligne de commande sous Linux

Le terminal qui permet à l'utilisateur d'interagir avec le système d'exploitation (donc avec la machine) 
se présente comme ceci (lancé par ```Ctrl + Alt + T```) :

![Emulateur de terminal](../images/terminal.png){width=30%}

Le terminal est une **interface en ligne de commande** (en anglais CLI : Command Line Interface).

Dans ce terminal s'éxecute un **interpréteur de commande** que l'on appelle ```shell``` (coquille autour du noyau Linux). Il existe différents shells, le plus commun est ```bash```, c'est celui qu'on utilisera.

Une **ligne de commande** est composée de texte écrit avec une syntaxe particulière que l'utilisateur saisit au clavier puis qu'il valide avec la touche `Entrée` pour demander à la machine d'effectuer une opération précise. L'ordinateur affiche alors le résultat de l'exécution de la commande saisie ou demande des précisions à l'utilisateur.

Il est également possible d'écrire ces commandes dans un fichier pour les réutiliser plus tard.  
On parle alors de **script** ``shell`` (ou de script `bash`).

Dans le terminal, lorsque l'interpréteur est prêt à recevoir une commande il l'indique par une **invite de commande** (appelée aussi en anglais *prompt*). Celle-ci se présente comme ceci :

![invite de commande](../images/invite.png){width=40%}

| Description de l'invite de commande | |
| ------------------ | ------------------------- | 
|slinux | nom de l'utilisateur (= **login**, le nom avec lequel l'utilisateur s'est connecté).|
|@ | séparateur, signifie ```at``` (en anglais *sur*). |
|slinux-pc | nom de la machine **sur** laquelle l'utilisateur s'est connecté. On peut se connecter à distance à la machine (un serveur ou un système embarqué par exemple).|
|:  | séparateur. |
|~ |  Le symbole ~ (tilde) est un raccourci pour préciser que l'on est dans le répertoire personnel de l'utilisateur ```/home/login``` ici: ```/home/slinux```, appelé aussi le répertoire de travail ``working directory``. |
|$  | l'utilisateur n'est qu'utilisateur. |
|#  | si à la place de $ est affiché # alors l'utilisateur est devenu (temporairement) administrateur (root) il a des droits de lecture et d'écriture supérieurs à ceux de simple utilisateur.|

### 2.5. Commandes essentielles

Au départ, le terminal affiche une **invite** de commande : ```slinux@slinux-pc:~$```  

Cette invite attend que l'utilisateur saisisse des commandes après le symbole `$`  

Ces commandes correspondent à des programmes qui s'exécutent sur l'ordinateur, puis rendent la "main" à l'utilisateur qui peut alors saisir de nouvelles commandes.

Exécuter sur le PC les commandes suivantes :

|Action à réaliser|Commande (+ 1 ou des options et arguments)|Résultat|
| ------------------ | ------------------------------ | ------------------------- |
|Connaître le répertoire courant | **pwd** | le répertoire de travail est : ```/home/slinux```|
|Changer de répertoire|**cd** Documents/|```slinux@slinux-pc:~/Documents$```|
|Aller dans le répertoire personnel|**cd** ~|```slinux@slinux-pc:~$```|
|Lister un répertoire|**ls**|liste des répertoires et fichiers du répertoire **courant**|
|Créer un répertoire| **mkdir** mon_dossier|`mon_dossier` créé|
|Créer un fichier| **touch** mon_fichier.txt|`mon_fichier.txt` créé|
|Copier un fichier| **cp** mon_fichier.txt copie.txt |`copie.txt` est la copie de `mon_fichier.txt`|
|Effacer un fichier| **rm** copie.txt |fichier effacé **définitivement**|
|Effacer un répertoire| **rm** -r mon_dossier|dossier effacé **définitivement**|


**Description des commandes essentielles**

|Commande|de l'anglais|Action|
| :----------- | --------------- | ---------------------------------------------------------------------------------------- |
|pwd        | `p`rint  `w`orking `d`irectory  | afficher le chemin complet du répertoire courant|
|cd         | ``c``hange ``d``irectory             | changer de répertoire = se déplacer dans l'arborescence|
|ls         | ``l``ist ``s``orted                  | afficher le contenu du répertoire courant par ordre alphabétique|
|mkdir      | `m`ake `dir`ectory               | créer un répertoire|
|cp         | `c`o`p`y                         | copier des fichiers ou des répertoires|
|rm         | `r`e`m`ove                       | effacer des fichiers ou des répertoires|
|mv         | `m`o`v`e                         | déplacer ou renommer des fichiers ou des répertoires|
|echo       |                                      | afficher un message ou le contenu d'une variable|
|cat        | con`cat`enate                      | visualiser le contenu d'un fichier ou la concaténation de plusieurs fichiers|
|man        | `man`ual                           | afficher les pages du manuel de la commande|
|touch      |                                      | réinitialiser le *timestamp* d'un fichier ou créer un fichier vide|

*timestamp* signifie horodatage donc quand on réinitialise le timestamp d'un fichier avec la commande `touch`, on modifie la date et l'heure de ce fichier. Si le fichier n'existe pas, la commande crée un fichier vide.

### 2.5.1. Les commandes essentielles (arguments)

Les commandes peuvent nécessiter un ou plusieurs **arguments**.

Exemple 1 : la commande `pwd` ne nécessite aucun argument
```bash
slinux@slinux-pc:~$ pwd
/home/slinux
```

Exemple 2 : la commande `cd` nécessite un seul argument 

```bash
slinux@slinux-pc:~/Documents$ cd mon_dossier/
```

Exemple 3 : la commande `mkdir` peut être employée avec 1 ou plusieurs arguments,  
ici on crée 2 dossiers avec la même commande

```bash
slinux@slinux-pc:~/Documents$ mkdir dossier1 dossier2
```

Vérification : on liste le contenu du dossier `Documents` avec la commande `ls`

```bash
slinux@slinux-pc:~/Documents/$ ls
dossier1  dossier2
```

Exemple 4 : la commande `touch` peut être employée avec 1 ou plusieurs arguments,  
ici on crée 3 fichiers avec la même commande

```bash
slinux@slinux-pc:~/Documents/dossier1$ touch index.html style.css script.js
```

Exemple 5 : la commande `cp` nécessite en argument le fichier à copier et le chemin du répertoire dans lequel écrire le fichier copie 

```bash
slinux@slinux-pc:~/Documents$ cp mon_fichier.txt dossier1/copie.txt
```

Exemple 6 : la commande `cp` nécessite en argument le fichier à copier et le fichier copie qui sera situé dans le même répertoire que le fichier copié

```bash
slinux@slinux-pc:~/Documents$ cp mon_fichier.txt copie.txt
```

### 2.5.2. Les commandes essentielles (options)

Les commandes peuvent être utilisées avec des **options** afin de préciser la tâche à effectuer.
Une option peut être soit une simple lettre, soit une lettre et une valeur. Une option est précédée d'un tiret `-` , parfois deux (ex. `cp --help`).

Exemple : la commande `ls -l` permet d'afficher les droits et permissions de tous les fichiers et répertoires du répertoire courant (on abordera les droits et permissions plus tard).

```bash
slinux@slinux-pc:~/Documents/dossier1$ ls -l
total 4
-rw-rw-r-- 1 slinux slinux 6 mai    4 22:44 copie.txt
-rw-rw-r-- 1 slinux slinux 0 mai    4 23:27 index.html
-rw-rw-r-- 1 slinux slinux 0 mai    4 23:27 script.js
-rw-rw-r-- 1 slinux slinux 0 mai    4 23:27 style.css
```
  
Exemple : lister un répertoire *avec ses fichiers cachés :* **ls *-a***  
**a** pour **a**ll : tous les fichiers, même cachés, seront affichés, les fichiers cachés ont un nom commençant par un `.`


**Exemple avec `rm`**  
**Attention, l'usage de `rm` est dangereux, soyez attentif !**

La commande `rm` permet de supprimer **définitivement** un fichier, il n'y a pas de corbeille dans le shell! Mais pas un répertoire non vide. 

Pour effacer tous les fichiers d'un répertoire :  

```bash
$ rm *
```

Et pour supprimer tout le contenu d'un répertoire, y compris les sous-répertoires, on utilise l'option `-r` (r pour récursivement) :

```bash
$ rm -r *
```


**À propos de la commande `grep`**  
La commande `grep` permet de rechercher une chaîne de caractères dans un fichier. La syntaxe générale est :

```bash
$ grep options "recherche" chemin
```

Voici quelques options utiles:

- `--ignore-case` ou `-i` : pour ignorer la casse (minuscules/majuscules indifférentes) ;
- `-c` : pour afficher seulement le nombre d'occurences de la recherche ;
- `-l` : pour afficher le nom des fichiers contenant la recherche (`chemin` est alors un répertoire) ;
- `-r` : pour rechercher dans tous les fichiers et sous-répertoires de `chemin`, qui est un répertoire.

Pour rechercher dans plusieurs fichiers on peut utiliser `*` qui remplace n'importe quel mot. Par exemple, pour rechercher `'password'` dans tous les fichiers `txt`, on peut taper:

```bash
$ grep 'password' *.txt
```

### 2.6. Naviguer dans une arborescence, les chemins

Pour aller dans son répertoire personnel en utilisant un **chemin absolu**,  
c'est-à-dire un chemin qui part toujours de la racine `/`, l'utilisateur *slinux* peut taper :  
`cd /home/slinux`

Il peut aussi utiliser le raccourci `~`, et faire `cd ~` ou plus simplement `cd`

Pour remonter dans le répertoire parent, on utilise `cd ..`  
et on désigne le répertoire courant par `.`

Ainsi, si on est dans le répertoire `dossier2` et que l'on veut copier le fichier `mon_fichier.txt` qui se trouve dans le répertoire parent `Documents` vers le répertoire courant (dossier2), on utilise la commande `cp` pour copier  
la SOURCE (`../mon_fichier.txt`) vers la DESTINATION (`.`) :

```bash
slinux@slinux-pc:~/Documents/dossier2$ cp ../mon_fichier.txt .
```

Résultat:

```bash
slinux@slinux-pc:~/Documents/dossier2$ ls
mon_fichier.txt
```

On a cette fois utilisé un **chemin relatif**, partant de la position actuelle (dossier2). 


### 2.7. La redirection de flux, le mécanisme

Le principe ? <br>
Utiliser la sortie d'une commande comme entrée d'une autre commande !

Toute la philosophie Unix tient dans le **pipe !** (tube)

Exemple :

**grep** : permet de vérifier des expressions régulières

**ls** : permet de lister un dossier

Donc, on peut **trouver les fichiers du répertoire courant** et qui contiennent "nsi" dans leur nom avec la commande suivante :

```bash
ls -al | grep "nsi" 
```


## 3. Droits et permissions sous Unix

Un système UNIX est un système multi-utilisateur. Toute personne physique ou tout programme interagissant avec le système est un **utilisateur** (user). Cet utilisateur est authentifié sur le système par un nom unique et un identifiant unique (UID). Chaque utilisateur possède certains droits lui permettant d'effectuer certaines opérations et pas d'autres (avoir accès aux répertoires et fichiers, aux périphériques, installer des logiciels...).

Pour connaître les utilisateurs du système, on consulte le fichier `/etc/passwd` où on trouve tous les utilisateurs au format :

```bash
nom:motdepasse:UID:GID:informations:repertoire:shell
```

Chaque utilisateur appartient à un ou plusieurs groupes, qui servent à rassembler plusieurs utilisateurs pour leur attribuer des droits (permissions d'accès) communs aux fichiers ou applications.

Pour connaître les utilisateurs du système, on consulte le fichier `/etc/group`

Parmi les utilisateurs, il y a un **super-utilisateur** appelé `root` qui a tous les pouvoirs sur le système. Son UID est 0. Pour exécuter une commande réservée au super-utilisateur, un utilisateur doit utiliser la commande `sudo` (super user do) qui nécessite de connaître le **mot de passe root** qui en général n'est connu que de l'administrateur du système.

En particulier, le super-utilisateur peut modifier les droits (en attribuer ou en retirer) des utilisateurs et des groupes.

**Les types de droits `r-w-x`**

- les droits en **lecture** (symbolisés par la lettre `r` pour **r**ead) : il est possible de lire le contenu de ce fichier
- les droits en **écriture** (symbolisés par la lettre `w` pour **w**rite) : il est  possible de modifier le contenu de ce fichier
- les droits en **exécution** (symbolisés par la lettre `x`pour e**x**ecute) : il est possible d'exécuter le contenu de ce fichier (quand le fichier est du code exécutable), quand c'est un répertoire, il est alors permis de le traverser.

**Les types d'utilisateurs `u-g-o`**
Tout fichier UNIX:  

- possède un propriétaire (par défaut l'utilisateur qui l'a créé) : **u** comme *user*;
- est associé à un groupe dont on définit les actions sur ce fichier: **g** comme *group*;
- peut être éventuellement manipulé par tous les autres utilisateurs : **o** comme *others*.

**Lecture des droits**  
Voici ce que donne la commande `ls` avec l'option `-l` pour obtenir des informations sur le contenu du répertoire `Documents` dont le chemin absolu est:  `/home/slinux/Documents`

![Commande ls -l](../images/terminal2.png){width=50%}

- le premier caractère `-` ou `d` indique s'il s'agit d'un fichier ou d'un répertoire (directory) ;
- les 9 caractères suivants représentent dans l'ordre les droits pour les 3 types d'utilisateurs (par paquets de 3 caractères), dans l'ordre **ugo**. Par exemple pour le fichier `mon_fichier.txt`, le propriétaire **u** a les droits `rw-`, c'est-à-dire lecture, écriture, pas d'éxécution (il ne s'agit pas d'un fichier exécutable), les utilisateurs du groupe ont les mêmes droits `rw-` et les autres utilisateurs **o** ont les droits `r--`, c'est-à-dire seulement lecture. Pour les dossiers, tous les utilisateurs ont le droit d'éxécution `x`, qui consiste à explorer le répertoire.
- ensuite on lit le nombre de liens (notion non étudiée cette année) ;
- on trouve ensuite le nom du propriétaire du fichier, le nom du groupe, la taille du fichier en octets, la date et l'heure de la dernière modification et enfin le nom du fichier ou du répertoire.

![Permissions](../images/permissions.png){width=50%} 


**Modification des droits**  
Il est important de ne pas perdre de vu que l'utilisateur `root` a la possibilité de modifier les droits de tous les utilisateurs.  
Le propriétaire d'un fichier peut également modifier les permissions d'un fichier ou d'un répertoire à l'aide de la commande `chmod`. 
Elle s'utilise ainsi, en précisant l'utilisateur (**a** pour tous), l'ajout **+** ou la suppression **-** ou la réinitialisation **=** de la permission et enfin le type de permission :

```bash
chmod [u g o a] [+ - =] [r w x] nom_du_fichier
```

Par exemple :  

```bash
chmod g+w toto.txt
```

attribuera la permission "écriture" au groupe associé au fichier `toto.txt`.

Fin pour l'instant...
