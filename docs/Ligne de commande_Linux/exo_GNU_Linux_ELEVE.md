---
title: "GNU/LINUX et ligne de commande"
author: []
date: "23 janvier 2023"
subject: "GNU / Linux"
keywords: [SE, Linux, ligne de commande]
lang: "fr"
header-left: NSI
header-center: GNU / LINUX ET SON INTERFACE EN LIGNE DE COMMANDE
header-right: Activités pratiques
footer-left: 
footer-center: "Thème: Architectures matérielles, système d'exploitation et réseaux"
footer-right: "Page \\thepage"
...

## 4. Exercices
    
**Exercice 1**  

**Partie 1**  

Décrire, sans les tester dans un terminal (donc avec papier/stylo), le résultat de chacune des commandes suivantes en supposant qu'elles sont exécutées les unes à la suite des autres.

1. `cd ~`
2. `mkdir NSI`
3. `mkdir NSI/TP_SHELL`
4. `cd NSI/TP_SHELL`
5. `touch toto.txt`
6. `ls -l`
7. `chmod g-rwx,o-rwx toto.txt`
8. `ls -l`
9. `cd ..`
10. `rm -r TP_SHELL`


**Partie 2**  
Ouvrir un terminal et exécuter ces commandes. Vérifier que tout se déroule comme décrit dans la partie 1.


**Exercice 2**  
  
Consulter la page de manuel d'utilisation de la commande `head`. Trouver comment l'utiliser pour n'afficher que les 5 premières lignes d'un fichier `toto.txt`.


**Exercice 3**

Récupérer [ce fichier](../data/foo.txt) `foo.txt` dans le dossier `data` et le copier dans le répertoire `/home/utilisateur/Téléchargements/`

Les instructions suivantes doivent **toutes** être réalisées à l'aide d'une (ou plusieurs si besoin) commande(s) dans le terminal. Moins de commandes seront utilisées, mieux ce sera.

1. Consulter le répertoire courant. Si ce n'est pas le répertoire personnel (`/home/utilisateur/`), s'y déplacer.
2. Créer un répertoire `Terminal/` dans le répertoire `Documents/`, et un répertoire `TP/` dans ce répertoire `Terminal/`.
3. Depuis le répertoire `Téléchargements/`, déplacer le fichier `foo.txt` vers le répertoire `TP/` qui vient d'être créé.
4. Renommer ce fichier en `queneau1014poemes.txt`.
5. Si le répertoire courant n'est pas `/home/utilisateur/Documents/Terminal/TP/`, s'y rendre. Sans quitter ce répertoire, créer un répertoire `Sauvegarde/` dans le répertoire `Terminal/`.
6. Depuis le répertoire `TP/`, créer une copie du fichier `queneau1014poemes.txt` dans le répertoire `Sauvegarde/` sous le nom `queneau_save.txt`. 
7. Depuis le répertoire `TP/`, consulter les 14 premières lignes du `queneau1014poemes.txt`. On pourra lire le manuel de la commande `head` en exécutant la commande `man head`, appuyer sur `q` pour quitter le manuel.
8. Parmi les 3 mots suivants, lequel(s) n'y figure(nt) pas (peu importe la casse) : Malabar, Bretzel, Cacahuète ? On pourra lire le manuel de la commande `grep`.
9. Donner le nombre d'occurences du mot «frère».
10. En quelle ligne trouve-t-on le mot «illustre» ?
11. On peut écrire du texte (par exemple «Bonjour») dans un fichier (par exemple «toto.txt») grâce à la commande `echo Bonjour > toto.txt`. Pour ajouter du texte, on remplace `>` par `>>`. Créer un fichier `reponses.txt` puis y écrire les réponses aux questions 6, 7 et 8.
12. Vérifier en affichant le contenu du fichier `reponses.txt`.
13. Consulter le contenu du répertoire `TP/`, en affichant les détails sur les droits.
14. Modifier les droits sur le fichier `reponses.txt` pour que les utilisateurs du groupe n'aient que la permission «lecture» et que les autres utilisateurs n'aient aucun droit d'accès.
15. Supprimer le répertoire `Terminal/` et tout son contenu.
16. Fermer le terminal.


**Exercice 4**

Récupérer [ce fichier](../data/pg6318.txt) `pg6318.txt` dans le dossier `data` et le copier dans le répertoire `/home/utilisateur/Téléchargements/`

Les instructions suivantes doivent **toutes** être réalisées à l'aide d'une (ou plusieurs si besoin) commande(s) dans le terminal. Moins de commandes seront utilisées, mieux ce sera.

1. Dans le répertoire personnel, créer un répertoire `Shell/` puis un répertoire `TP/` dans ce répertoire `Shell/`.
2. Déplacer le fichier `pg6318.txt` dans le répertoire `TP/` qui vient d'être créé.
3. Créer une copie de sauvegarde de ce fichier dans le répertoire `Shell/`.
4. Consulter les 25 premières lignes de ce fichier, puis le-renommer *judicieusement*.
5. Créer un fichier nommé `reponses.txt`.
6. Écrire son nom dans ce fichier à l'aide de la commande `echo`.
7. Chercher les mots `rouge`, `bleu` et `jaune` dans le fichier texte et ajouter au fichier `reponses.txt` celui qui n'y apparaît pas.
9. Vérifier en affichant le contenu du fichier `reponses.txt`.
8. Chercher en quelle ligne apparaît pour la première fois le mot `traître`.
10. Consulter le contenu du répertoire `Shell/`, en affichant les détails sur les droits.
11. Ajouter un droit d'écriture aux «autres utilisateurs» sur le répertoire `TP/`. 
12. Supprimer le droit d'écriture sur le fichier `reponses.txt` pour les utilisateurs du groupe.
13. Supprimer le répertoire `Shell/` et tout son contenu.
14. Consulter le manuel de la commande `top`.
15. Vérifier que Thonny est bien lancé sur la VM Xubuntu. Sinon, le lancer, puis taper la commande `top` et noter le PID de Thonny. Appuyer sur `q` pour quitter.
16. Taper la commande `kill -9 [PID]` où il faut remplacer `[PID]` par le PID de Thonny.
17. Fermer le terminal.
