# Pyodide

On peut avoir un IDE avec un fichier Python préchargé.

Avec un fichier `exemple.py` qui se trouve dans le même dossier que votre `page.md` à créer.

On ajoute une ligne avec `{{ "{{ IDE('exemple') }}" }}`

{{ IDE('exemple') }}

<!-- ci-dessous fonctionne pas car image bouton transparente, rajout d'un fond nécessaire
Il dispose de six boutons :

- Lancer le script : [](/docs/images/buttons/icons8-play-64.png)

<div>
<ul>
<li> Lancer le script : <button class="tooltip"><img src="/docs/images/buttons/icons8-play-64.png"></button></li>
<li> Valider le script avec des tests unitaires : <button class="tooltip"><img src="../images/icons8-check-64.png"></button></li>
<li> Télécharger le script actuel : <button class="tooltip"><img src="../images/icons8-download-64.png"></button></li>
<li> Téléverser un script local : <button class="tooltip"><img src="../images/icons8-upload-64.png"></button></li>
<li> Recharger l'énoncé : <button class="tooltip"><img src="../images/icons8-restart-64.png"></button></li>
<li> Enregistrer le script actuel : <button class="tooltip"><img src="../images/icons8-save-64.png"></button></li>
</ul>
</div> -->


{{ IDEv('exemple') }}

{{ terminal() }}

??? info "Rappel RGPD"

    Tout se fait du côté client. **Rien n'est envoyé au serveur**.


Pour de plus amples informations, le travail de Vincent Bouillot est présenté ici : <https://bouillotvincent.gitlab.io/pyodide-mkdocs/>
