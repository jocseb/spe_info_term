# Recherche textuelle

On part d'un texte dans lequel on cherche un motif.

## 1. Recherche naïve

!!! tip "Illustration de l'algorithme"
    <gif-player src="https://jocseb.gitlab.io/spe_info_term/Algorithmique/Recherche_textuelle/data/gif_naive.gif" speed="1" play></gif-player>

    _On peut contrôler le déroulement de l'animation en la survolant avec le curseur de la souris de gauche à droite._

    _En immobilisant le curseur, on met en pause l'animation._

    _Légende : caractère courant du motif en bleu, caractère du texte différent de celui dans le motif en rouge, caractère du texte identique au caractère courant du motif en vert_


### 1.1 L'approche "naïve"

[codes à trous](../intro_naive/){. target="_blank"}

!!! example "Exercice 1: algorithme de recherche naïve"
    === "Énoncé"
        Écrire une fonction ```recherche_naive``` qui prend en paramètres deux chaines de caractères ```texte``` et ```motif``` et qui renvoie la liste des indices (éventuellement vide) des occurrences de la chaîne `motif` dans la chaîne `texte`.

    === "Correction"
        ```python linenums='1'
        def recherche_naive(texte, motif):
            """ renvoie la liste (éventuellement vide) des indices des occurrences de
            de la chaîne motif dans la chaîne texte. """
            indices = []
            i = 0
            while i <= len(texte) - len(motif):
                k = 0
                while k < len(motif) and texte[i+k] == motif[k]:
                    k += 1
                if k == len(motif):
                    indices.append(i)
                i += 1

            return indices
        ```

Exemples d'utilisation :
```python
>>> recherche_naive("une magnifique maison bleue", "maison")
[15]
>>> recherche_naive("une magnifique maison bleue", "nsi")
[]
>>> recherche_naive("une magnifique maison bleue", "ma")
[4, 15]
```


### 1.2 Modification de l'algorithme

!!! example "Exercice 2"
    === "Énoncé"
        Créer une nouvelle fonction ``recherche_naive_bool`` qui s'arrête dès qu'une occurrence de ```motif``` est trouvée dans ```texte```.

        Cette fonction renverra uniquement un booléen : ``trouve``. 

    === "Correction"
        ```python linenums='1' hl_lines='4 6 11 14'
        def recherche_naive_bool(texte, motif):
            """ renvoie un booléen indiquant la présence ou non de
            la chaîne motif dans la chaîne texte. """
            trouve = False
            i = 0
            while i <= len(texte) - len(motif) and not trouve:
                k = 0
                while k < len(motif) and texte[i+k] == motif[k]:
                    k += 1
                if k == len(motif):
                    trouve = True
                i += 1
            
            return trouve
        ```

### 1.3 Application à la recherche d'un motif dans un roman

Le [Projet Gutenberg](https://www.gutenberg.org/browse/languages/fr){. target="_blank"} permet de télécharger légalement des ouvrages libres de droits dans différents formats.

On va travailler avec le Tome 1 du roman _Les Misérables_ de Victor Hugo, à télécharger [ici](data/Les_Miserables.txt){. target="_blank"} au format ```txt```. 

#### 1.3.1 Récupération du texte dans une seule chaîne de caractères

```python
with open('Les_Miserables.txt', encoding="utf-8") as f:
    roman = f.read().replace('\n', ' ')
```

Dans la suite de ce document, ``roman`` sera le ``texte`` dans lequel on recherchera le ``motif``.

#### 1.3.2 Vérification et mesure du temps de recherche

!!! example "Exercice 3"
    === "Énoncé"
        À l'aide du module ```time```, mesurer le temps de recherche de la fonction ``recherche_naive`` dans Les Misérables d'un mot court (``maison``), d'une longue phrase présente dans le texte (``La chandelle était sur la cheminée et ne donnait que peu de clarté.``), d'un mot qui n'existe pas dans le roman (``informatique``). Que remarque-t-on ? 

    === "Correction"
        ```python linenums='1'
        t0 = time.time()
        motif = 'maison'
        print(recherche_naive(roman, motif))
        print(time.time()-t0)

        t0 = time.time()
        motif = 'La chandelle était sur la cheminée et ne donnait que peu de clarté.'
        print(recherche_naive(roman, motif))
        print(time.time()-t0)

        t0 = time.time()
        motif = 'informatique'
        print(recherche_naive(roman, motif))
        print(time.time()-t0)  
        ```

        On remarque que le temps de recherche est semblable, quel que soit le motif cherché. 

## 2. Vers l'algorithme de Boyer-Moore : et si on partait à l'envers ?

???+ tip "Illustration de l'algorithme en partant à l'envers"
    <gif-player src="https://jocseb.gitlab.io/spe_info_term/Algorithmique/Recherche_textuelle/data/naive_envers.gif" speed="1" play></gif-player>

    _On peut contrôler le déroulement de l'animation en la survolant avec le curseur de la souris de gauche à droite._

    _En immobilisant le curseur, on met en pause l'animation._

    _Légende : caractère courant du motif en bleu, caractère du texte différent de celui dans le motif en rouge, caractère du texte identique au caractère courant du motif en vert_

!!! example "Exercice 4"
    === "Énoncé"
        Programmer une fonction ``presqueBMH`` en s'inspirant de l'algorithme de recherche naïve mais en démarrant le parcours de la **fin** du motif et non du début. 

    === "Correction"
        ```python linenums='1' hl_lines='5-8'
        def presqueBMH(texte, motif):
            indices = []
            i = 0
            while i <= len(texte) - len(motif):
                k = len(motif) - 1 # on commence par la fin du motif
                while k > 0 and texte[i+k] == motif[k]:
                    k -= 1
                if k == 0:
                    indices.append(i)
                i += 1

            return indices
        ```

## 3. Une approche plus élaborée: algorithme de Boyer-Moore-Horspool

### 2.1 Principe

L'algorithme de Boyer-Moore-Horspool compare la dernière lettre du motif recherché avec celles du texte en tenant compte de leur présence ou non dans le
motif recherché pour établir des décalages pertinents et réduire par là-même le nombre de comparaisons à effectuer afin de retrouver la position du motif dans le texte.

L'idée est d'améliorer le code précédent (celui où on parcourt le motif à l'envers) en **se décalant** directement au prochain endroit potentiellement valide.

Pour cela, on regarde le caractère ```X```  du texte sur lequel on s'est arrêté (car ```X``` n'ést pas égal au caractère de rang équivalent dans le motif):

- si ```X``` n'est pas dans le motif, il est inutile de se déplacer "de 1" : on retomberait tout de suite sur ```X```, c'est du temps perdu. On se décale donc juste assez pour dépasser ```X```, donc de la longueur du motif cherché.
- si ```X``` est dans le motif (sauf à la dernière place du motif !), on va regarder la place de la dernière occurence de ```X``` dans le motif et **se déplacer de ce nombre**, afin de faire coïncider le ```X``` du motif et le ```X``` du texte.

???+ tip "Illustration de l'algorithme"
    <gif-player src="https://jocseb.gitlab.io/spe_info_term/Algorithmique/Recherche_textuelle/data/gif_BM.gif" speed="1" play></gif-player>

    _On peut contrôler le déroulement de l'animation en la survolant avec le curseur de la souris de gauche à droite._

    _En immobilisant le curseur, on met en pause l'animation._
    
    _Légende : caractère courant du motif en bleu, caractère du texte différent de celui dans le motif en rouge, caractère du texte identique au caractère courant du motif en vert, caractère du texte présent dans le motif en violet_


### 2.2 Implémentation

#### 2.2.1 Fonction préparatoire

On va d'abord coder une fonction ```dico_lettres``` qui prend en paramètre un motif ```motif``` et qui renvoie un dictionnaire associant à chaque lettre de ```motif``` son dernier rang dans ```motif``` par rapport à la **dernière lettre**. On exclut la dernière lettre, qui poserait un problème lors du décalage (on décalerait de 0...).

**Remarque** : si 2 mêmes lettres sont présentent dans le motif, le rang retenu est celui de la dernière lettre.
Exemple avec le motif ``ulysse``, la fonction ```dico_lettres``` doit renvoyer : ``{'u': 5, 'l': 4, 'y': 3, 's': 1}``

Pour implémenter cela, on se servira de la propriété suivante des dictionnaires en python :

- si 2 valeurs sont saisies pour la même clé alors la 2de valeur saisie écrase la 1ère.

!!! example "Exercice 5"
    === "Énoncé"
        Écrire la fonction ```dico_lettres```.

        *Exemple d'utilisation :*

        ```python
        >>> dico_lettres("TAG")
        {'T': 2, 'A': 1}
        ```

    === "Correction"
        ```python linenums='1'
        def dico_lettres(motif):
            decalages = {}
            for i in range(len(motif)-1): # la dernière lettre n'apporte rien
                lettre = motif[i]
                decalages[lettre] = len(motif) - 1 - i
            return decalages
        ```

#### 2.2.2 Boyer-Moore-Horspool

<!-- [codes à trous](../intro_BMH/){. target="_blank"} -->

L'algorithme de Boyer-Moore-Horspool consiste, pour une position donnée, à comparer la dernière lettre du motif avec celle du texte 
(de sorte que lorsqu'elles correspondent on procède à la vérification du mot tout entier) 
et à appliquer ensuite un décalage menant à une correspondance des lettres lorsque c'est possible.

!!! example "Exercice 6: algorithme de Boyer-Moore-Horspool"
    === "Énoncé"
        Compléter le code suivant :

        ```python linenums='1'
        def dico_lettres(motif):
            pass

        def BMH(texte, motif):
            dico = dico_lettres(motif) # décalage pré-calculé
            indices = []
            i = 0
            while i <= len(texte) - len(motif):
                k = len(motif) - 1
                while k >= 0 and texte[...] == motif[...]: # (1)
                    k -= 1
                if k == ...: # (2)
                    indices.append(i)
                    i += ... # (3)
                else:
                    lettre_courante = ...      # (4)
                    if lettre_courante in ...: # (5)
                        i += dico[...] # (6)
                    else:
                        i += len(motif) # (7)

            return indices
        ```

        1. On remonte le motif à l'envers (de droite à gauche), tant qu'il y a correspondance et qu'on n'est pas arrivé au début du motif
        2. Si on est arrivé au début du motif, c'est qu'on a trouvé le motif.
        3. On a trouvé le motif, mais attention, il ne faut pas trop se décaler sinon on pourrait manquer d'autres occurences du motif (exemple: la recherche du motif «mama» dans le mot «mamamamama»). On se décale donc de 1.
        4. On s'est arrêté avant la fin, la lettre courante du texte est peut-être présente dans le motif : on la garde en mémoire.
        5. Si la lettre courante du texte est présente dans le motif : il va falloir faire un décalage à droite du motif vers cette lettre avec le décalage pré-calculé.
        6. On décale juste de ce qu'il faut pour mettre en correspondance la lettre du motif avec celle du texte.
        7. La lettre n'est pas présente dans le motif : on se positionne en se décalant de toute la longueur du motif. 

    === "Correction"
        ```python linenums='1'
        def dico_lettres(motif: str) -> dict:
            decalages = {}
            for i in range(len(motif)-1): # la dernière lettre n'apporte rien
                lettre = motif[i]
                decalages[lettre] = len(motif) - 1 - i
            return decalages
            
        def BMH(texte: str, motif: str) -> list:
            dico = dico_lettres(motif) # décalage pré-calculé
            indices = []
            i = 0
            while i <= len(texte) - len(motif):
                k = len(motif) - 1
                while k >= 0 and texte[i+k] == motif[k]: #(1)
                    k -= 1
                if k == -1: #(2)
                    indices.append(i)
                    i += 1 #(3)
                else:
                    lettre_courante = texte[i + k] #(4)
                    if lettre_courante in dico: #(5)
                        i += dico[lettre_courante] #(6)
                    else:
                        i += len(motif) #(7)

            return indices
        ```

Exemple d'utilisation :
```python
>>> BMH("une magnifique maison bleue", "maison")
[15]
>>> BMH("une magnifique maison bleue", "nsi")
[]
>>> BMH("une magnifique maison bleue", "ma")
[4, 15]
```

!!! example "Exercice 7"
    Reprendre les mesures effectuées sur Les Misérables, mais cette fois avec la fonction BMH. Que remarque-t-on ?  
   
    === "Correction"
        ```python linenums='1'
        t0 = time.time()
        motif = 'maison'
        print(BMH(roman, motif))
        print(time.time()-t0)

        t0 = time.time()
        motif = 'La chandelle était sur la cheminée et ne donnait que peu de clarté.'
        print(BMH(roman, motif))
        print(time.time()-t0)

        t0 = time.time()
        motif = 'informatique'
        print(BMH(roman, motif))
        print(time.time()-t0)
        ```

        Résultats dans la console :

        ```python
        [7264, 9090, 9547, 9745, 10936, 17820, 23978, 38192, 41639, 41651, 41840, 42493, 48028, 48393, 51448, 53353, 70867, 72692, 72768, 75608, 77855, 108489, 115739, 130629, 132983, 138870, 143681, 144600, 153114, 155973, 158709, 160700, 163649, 169164, 169181, 171761, 171967, 182642, 186413, 190534, 219378, 220314, 224518, 225098, 227579, 296302, 345108, 345893, 346740, 349677, 359727, 362025, 389945, 395690, 434118, 438068, 457795, 457886, 464696, 469403, 501768, 514980, 520667, 520878, 520926, 520968, 522707, 529329, 598128, 601390, 645915]
        0.07081079483032227
        [651731]
        0.01998758316040039
        []
        0.042844295501708984
        ```

        On constate quelque chose de remarquable (et qui peut être à première vue contre-intuitif) : 

        **Plus le motif recherché est long, plus la recherche est rapide**.        
  

**Remarque**: l'algorithme de Boyer-Moore-Horspool est une version simplifiée de l'algorithme de Boyer-Moore qui utilise deux tables de décalage.
On appelle l'algorithme de Boyer-Moore-Horspool aussi l'algorithme de Boyer-Moore simplifié.