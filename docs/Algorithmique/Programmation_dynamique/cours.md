# Programmation dynamique
 
## 1. Retour sur la suite de Fibonacci

### 1.1 Simple et inefficace

Comme nous l'avons déjà vu, la suite de Fibonacci est définie par une formule de récurrence qui dépend des deux termes précédents :

- $f_0 = 0$
- $f_1 = 1$
- $f_n = f_{n-1} + f_{n-2}$ pour $n\ge2$

et elle se programme récursivement par :

```python linenums='1'
def fib1(n):
    if n <= 1:    # cas de base
        return n
    else:         # cas récursif
        return fib1(n-1) + fib1(n-2)
```

C'est la 1ère fonction de Fibonacci qu'on a programmé dans le chapitre ``Récursivité``. 

On peut aussi l'écrire sans le ``else`` :

```python linenums='1'
def fib1(n):
    if n <= 1:    # cas de base
        return n
    # cas récursif
    return fib1(n-1) + fib1(n-2)
```

Ce code, d'une grande simplicité, est malheureusement très inefficace.

!!! example "Exercice 1"
    === "Énoncé"
        Mesurer le temps de calcul de ```fib1(38)``` en vous aidant de ce qui a été fait dans le chapitre **récursivité**.

    === "Correction"
        ```python linenums='1'
        import timeit

        def fib1(n):
            if n <= 1:
                return n   
            return fib1(n-1) + fib1(n-2)

        print("Durée d'exécution de fib1(38) :", end= " ")
        print(timeit.timeit("fib1(38)", setup="from __main__ import fib1", number=1))
        ```
        Le temps de calcul est de plus de dix secondes, sur une machine récente. C'est très mauvais !

En cause : la multitude des appels récursifs nous conduit à refaire des calculs déjà effectués.

Observons l'arbre d'appels de ```fib1(5)``` :

![image](data/Arbre_des_appels_fib1_5.png)

Le calcul de ```fib1(1)``` se retrouve ainsi 5 fois dans l'arbre et le calcul de ```fib1(2)``` s'y' retrouve 3 fois.

Pour résoudre notre problème, nous l'avons divisé en problèmes plus petits, mais malheureusement pas indépendants : on dit que les problèmes se **recouvrent**, ce qui nous amène à résoudre les mêmes problèmes plusieurs fois.

Dans l'algorithme de dichotomie, ou du tri-fusion, les problèmes étaient indépendants et ne se recouvraient pas : on ne refaisait jamais deux fois la même chose. Ce n'est pas le cas ici.

### 1.2 Se souvenir des calculs : la mémoïsation

Comment éviter de recalculer (par exemple) 3 fois ```fib1(2)``` ?

L'idée générale est de stocker le résultat de chaque calcul, par exemple dans un dictionnaire. Ainsi, à chaque demande de calcul :

- soit le calcul a déjà été effectué : on a donc juste à le lire dans le dictionnaire ;
- soit le calcul n'a jamais été effectué : on l'effectue donc et **on stocke le résultat dans le dictionnaire**.

!!! example "Exercice 2"
    === "Énoncé"
        Compléter le code suivant :

        ```python linenums='1'
        dict_fibo = {0:0, 1:1}
        def fibo(n):
            if n in dict_fibo:
                return ...
            dict_fibo[n] = ... + ...
            return ...
        ```

    === "Correction"
        ```python linenums='1'
        dict_fibo = {0:0, 1:1}
        def fibo(n):
            # le calcul a déjà été effectué, on lit le dico puis renvoie le résultat
            if n in dict_fibo:
                return dict_fibo[n]
            # le résultat d'un nouveau calcul est stocké dans le dico et renvoyé
            dict_fibo[n] = fibo(n-1) + fibo(n-2)
            return dict_fibo[n]
        ```        

!!! example "Exercice 3"
    === "Énoncé"
        Mesurer le temps de calcul de ```fibo(38)``` et comparer avec la mesure de l'exercice 1.

    === "Correction"
        Le temps de calcul est maintenant de l'ordre de $10^{-5}$ secondes. C'est un million (!!!) de fois plus rapide qu'à l'exercice 1.
   

### 1.3 Quelques remarques

#### 1.3.1 **Juste une brute-force plus efficace ?**

Notre technique de mémoïsation ne change pas vraiment la structure du programme : on continue de calculer toutes les valeurs intermédiaires, **mais on ne les calcule qu'une seule fois.**

#### 1.3.2 **Suppression de la variable globale**

Dans le code précédent, le dictionnaire ```dict_fibo``` est à l'extérieur de la fonction. Un dictionnaire étant un type mutable, sa modification à l'intérieur de la fonction ne pose pas de problème. Toutefois, ce genre de pratique est déconseillé : si par exemple on appelle 2 fois la fonction ```fibo```, le dictionnaire n'est pas réinitialisé entre les 2 appels (ce qui dans notre cas n'est pas problématique, mais cela pourrait l'être). Comment éviter cela ?

On peut utiliser une fonction *englobante* (appelée ici ```fibonacci``` ) et la fonction ``fibo`` est appelée fonction locale car elle est définie à l'intérieur d'une autre mais elle n'existe pas à l'extérieur :

```python linenums='1'
def fibonacci(n):
    dict_fibo = {0:0, 1:1}
    def fibo(n):
        if n in dict_fibo:
            return dict_fibo[n]
        dict_fibo[n] = fibo(n-1) + fibo(n-2)
        return dict_fibo[n]
    return fibo(n)
```

```python
>>> fibonacci(50)
12586269025
```

On peut aussi écrire, en enlevant la 2ème ligne :

```python linenums='1'
def fibonacci(n):
    dict_fibo = dict()
    def fibo(n):
        if n in dict_fibo:
            return dict_fibo[n]
        if n <= 1:
            dict_fibo[n] = n
        else:
            dict_fibo[n] = fibo(n-1) + fibo(n-2)
        return dict_fibo[n]
    return fibo(n)
```

#### 1.3.3 **Mémoïsation automatique en python**

Il existe dans ``python`` un décorateur de mémoïsation: ```lru_cache``` du module ```functools``` que l'on peut utiliser directement pour mémoïser une fonction récursive automatiquement.

```python linenums='1'
import timeit
from functools import lru_cache

@lru_cache(maxsize=None) #(1)
def fibo_cache(n): #(2)
    if n <= 1:
        return n   
    return fibo_cache(n-1) + fibo_cache(n-2)
    
print("Durée d'exécution de fibo_cache(38) :", end= " ")
print(timeit.timeit("fibo_cache(38)", setup="from __main__ import fibo_cache", number=1))
```

1. «décorateur» de mémoïsation de la fonction
2. ceci est notre VIEILLE fonction ```fib1```, extrêmement lente...

Essayez en commentant / décommentant la ligne 4... c'est magique !

### 1.4 De bas en haut

La structure récursive naturelle de la suite de Fibonacci nous a conduit vers un programme qui calcule (ou plutôt *appelle*) les valeurs *de haut en bas* (méthode appelée *top-down* ou descendante).

Et si on commençait par le bas ?

Si nous devions calculer mentalement le 6ème terme de la suite de Fibonacci, on commencerait par calculer le 3ème, puis le 4ème, puis le 5ème et enfin le 6ème.
On stocke les résultats intermédiaires de proche en proche dans un tableau ou un dictionnaire comme dans la mémoïsation.


!!! example "Exercice 4"
    === "Énoncé"
        Compléter le code ci-dessous :

        ```python linenums='1'
        def fibo(n):
            dict_fibo = {}
            dict_fibo[0] = ...
            dict_fibo[1] = ...
            for k in range(..., ...):
                dict_fibo[k] = dict_fibo[...] + dict_fibo[...]
            return dict_fibo[...]
        ```

    === "Correction"
        ```python linenums='1'
            def fibo(n):
                dict_fibo = {}
                dict_fibo[0] = 0
                dict_fibo[1] = 1
                for k in range(2, n+1):
                    dict_fibo[k] = dict_fibo[k-1] + dict_fibo[k-2]
                return dict_fibo[n]
        ```        

Cette méthode **itérative** part du bas pour aller vers le haut. On parle de méthode *bottom-up* ou méthode ascendante.
De manière plus générale, cette méthode est basée sur le fait de résoudre les sous-problèmes de petite taille, puis ceux de taille intermédiaire... jusqu'à la taille voulue c'est-à-dire le problème final.

### 1.5 Bilan des méthodes employées

!!! note "Principes de programmation dynamique"

    - Lors d'un calcul effectué de manière récursive, il peut arriver que de multiples appels récursifs soient identiques. Pour éviter de recalculer plusieurs fois la même chose, on peut stocker les résultats intermédiaires. On appelle cette technique la **mémoïsation**.  
    Cette technique minimise le nombre d'opérations et accélère grandement l'exécution du programme. Le prix à payer est l'utilisation d'une structure de stockage des valeurs intermédiaires, et donc une **augmentation de la mémoire** utilisée par le programme.

    - Lors d'un calcul effectué de manière itérative, il est parfois plus simple de commencer par une «petite» version du problème pour progressivement remonter vers la solution du problème global.

## 2. Programmation dynamique et optimisation

### 2.1 Optimisation du rendu de monnaie

**Problème**

![image](data/francs.png){width=70%}

Étant données une liste de pièces ```pieces``` et une somme à rendre ```somme```, peut-on calculer le nombre minimal de pièces pour réaliser cette somme ?

