def rendu_glouton(pieces: list, somme: int):
    i =  len(pieces) - 1
    nb_pieces = 0
    while somme > 0:
        if pieces[i] <= somme:
            nb_pieces = nb_pieces + 1 
            somme = somme - pieces[i] 
        else:
            i = i - 1   
    return nb_pieces


pieces = [20, 10, 5, 2, 1]
s = 6
# Liste de toutes les façons de rendre la somme 6:
# 5+1; 2+2+2; 2+2+1+1; 2+1+1+1+1; 1+1+1+1+1+1
# il y a donc 5 façons de rendre cette somme
print(rendu_glouton(pieces, s)) # erreur trouve 6


def rendu(pieces, somme):
    if somme < 0:
        return 0
    if not pieces:
        return 0
    if somme == 0:
        return 1
    return rendu(pieces, somme - pieces[0]) + rendu(pieces[1:], somme)

print(rendu(pieces, s)) # renvoie bien 5


def rendu_recursif(pieces, somme):
    nb_pieces = somme
    if somme == 0:
        return 0
    for p in pieces:
        if p <= somme:
            nb_pieces = min(nb_pieces, 1 + rendu_recursif(pieces, somme-p))
    return nb_pieces    

print(rendu_recursif(pieces, s)) # renvoie 2


memo_rendu = {}
def rendu_recursif_memoise(pieces, somme):
    nb_pieces = somme
    if somme == 0:
        return 0
    if somme in memo_rendu:
        return memo_rendu[somme]
    for p in pieces:
        if p <= somme:
            nb_pieces = min(nb_pieces, 1 + rendu_recursif_memoise(pieces, somme-p))
    memo_rendu[somme] = nb_pieces
    return memo_rendu[somme]  

#rendu_recursif_memoise([1, 6, 10], 107)

############################################################
# liste des pièces
############################################################
def rendu_solution(pieces, somme):
    rendu = {0:0}
    solution = {}
    solution[0] = []
    for s in range(1, somme+1):
        rendu[s] = s
        solution[s] = []
        for p in pieces:
            if p <= s:
                if 1 + rendu[s-p] < rendu[s]:
                    rendu[s] = 1 + rendu[s-p]
                    solution[s] = solution[s-p].copy()
                    solution[s].append(p)
    return solution[somme]
