# Optimisation de rendu de monnaie

!!! question "Problème"
    ![image](data/francs.png)

    Nous allons nous intéresser au problème suivant :

    Étant données une somme à rendre ```somme``` et une liste de pièces ```pieces```, peut-on calculer le nombre minimal de pièces pour réaliser cette somme ?

**Remarques importantes :**

- Dans toute la suite, on considérera que la somme à rendre est un nombre entier positif, et que dans la liste de pièces se trouve la pièce de valeur 1. Ainsi, il est **toujours possible** de rendre la monnaie.
- Notez bien que tous nos futurs algorithmes vont chercher à donner **le nombre de pièces rendues** et pas la composition de celles-ci.

## 1. Retour sur l'algorithme glouton

Nous avons vu en classe de première, un algorithme capable de donner une combinaison de pièces pour rendre la somme ```somme```.

Cet algorithme fonctionnait de manière **gloutonne** : on cherche à rendre à chaque fois la pièce de plus grande valeur possible.

!!! example "Exercice 1"
    === "Énoncé"
        Compléter la fonction ```rendu_glouton``` qui prend en paramètre la somme à rendre ```somme``` et une liste de pièces ```pieces``` (classées dans l'ordre croissant) et qui renvoie **le nombre minimal** de pièces qu'il faut rendre.

        ```python linenums='1'
        def rendu_glouton(somme: int, pieces: list) -> int:
            """combien de pièces faut-il pour obtenir la somme ?
            Le tableau pieces contient les valeurs des pièces à disposition dans l'ordre croissant."""
            i =  ... # (1)
            nb_pieces = ...
            while ... > ... and i < len(pieces):
                valeur = pieces[i] # (2)
                if ... <= somme:
                    nb_pieces = ... + ...
                    somme = ... - ...
                else:
                    i = ... - ...  
            return ...
        ```

        1. Attention, les pièces sont classées dans l'ordre **croissant**.
        2. on prend la pièce d'indice i

        *Exemple d'utilisation :*
        ```python
        >>> rendu_glouton(12, [1, 2, 5])
        3
        ``` 

Nous savons que cet algorithme est optimal sous certaines conditions sur la composition des pièces. Par exemple le système des euros (1, 2, 5, 10, 20, 50, 100, 200) rend l'algorithme glouton optimal (on dit que le système est *canonique*).

Mais si le système n'est pas canonique, l'algorithme glouton peut ne pas donner la meilleure solution :

```python
>>> rendu_glouton(12, [1, 6, 10])
3
```

Notre algorithme va trouver que $12 = 10 + 1 + 1$ et donc rendre 3 pièces, alors qu'il est possible de faire $12 = 6 + 6$ et ne rendre que 2 pièces.

## 2. Algorithme récursif

Il est possible de construire un algorithme optimal de manière récursive.

Il faut pour cela faire les observations suivantes :

- pour rappel, le rendu est toujours possible : **dans le pire des cas**, le nombre de pièces à rendre est égal à la somme de départ (rendu effectué à coups de pièces de 1)
- Si ```piece``` est une pièce de ```pieces```, le nombre minimal de pièces nécessaires pour rendre la somme ```somme```   est égal à 1 + le nombre minimal de pièces nécessaires (contenant ```piece```)    pour rendre la somme ```somme - piece```.

Cette dernière observation est cruciale. Elle repose sur le fait qu'il suffit d'ajouter 1 pièce (la pièce de valeur ```piece```) à la meilleure combinaison qui rend ```somme - piece``` pour avoir la meilleure combinaison qui rend ```somme``` (meilleure combinaison parmi celles contenant ```piece```).

On va donc passer en revue toutes les pièces ```piece``` et mettre à jour à chaque fois le nombre minimal de pièces.

!!! example "Exercice 2"
    === "Énoncé"
        Compléter la fonction ```rendu_recursif``` qui prend en paramètre la somme à rendre ```somme``` et une liste de pièces ```pieces``` et qui renvoie **le nombre minimal** de pièces qu'il faut rendre.

        ```python linenums='1'
        def rendu_recursif(somme: int, pieces: list) -> int:
            nb_pieces = ... # (1)
            if somme == 0:
                return ... # (2)
            for piece in pieces:
                if ... <= ...: # (3)
                    nb_pieces = min(nb_pieces, ... + rendu_recursif(..., pieces))
            return ...   
        ```        

        1. nombre de pièces dans le pire des cas
        2. cas de base
        3. peut-on rendre cette pièce ? 

Testons notre algorithme :

```python
>>> rendu_recursif(12, [1, 2, 5])
3
>>> rendu_recursif(12, [10, 6, 1])
2
```

Il ne se laisse pas pièger comme l'algorithme glouton et rend bien en 2 pièces pour la somme 12.

Mais...

```python
>>> rendu_recursif(107, [1, 6, 10])
RecursionError: maximum recursion depth exceeded in comparison
```

Le nombre d'appels récursifs de notre algorithme augmente exponentiellement avec la valeur de la somme à rendre : on se retrouve très rapidement avec des milliards d'appels récursifs, ce qui n'est pas gérable.

Ces appels récursifs ont lieu sur un nombre limité de valeurs : par construction de notre algorithme, si la somme à rendre est 100, il y aura beaucoup (beaucoup) d'appels vers 99, vers 98, vers 97... jusqu'à 0.

On peut donc légitimement penser à **mémoïser** notre algorithme, en stockant les valeurs pour éviter de les recalculer.

## 3. Algorithme récursif memoïsé

!!! example "Exercice 3"
    === "Énoncé"
        Compléter la fonction ```rendu_recursif_memoise``` qui prend en paramètre la somme à rendre ```somme``` et une liste de pièces ```pieces``` et qui renvoie **le nombre minimal** de pièces qu'il faut rendre.

        On utilisera le dictionnaire ```memo_rendu``` dans lequel on associera à chaque somme ```somme``` son nombre de pièces minimal. 

        On procèdera de manière classique :

        - soit la ```somme``` est disponible dans le dictionnaire, et on se contente de renvoyer la valeur associée ;
        - soit on la calcule (comme dans l'algorithme classique), puis on stocke le résultat dans le dictionnaire avant de le renvoyer.
        
        ```python linenums='1'
        memo_rendu = {}
        def rendu_recursif_memoise(somme: int, pieces: list) -> int:
            nb_pieces = somme
            if somme == 0:
                return 0
            if ... in ...:
                return ...
            for piece in pieces:
                if piece <= somme:
                    nb_pieces = ...
            ... = ...
            return ...
        ```

Notre algorithme est maintenant beaucoup (beaucoup) plus efficace :

```python
>>> rendu_recursif_memoise(12, [10, 6, 1])
2
>>> rendu_recursif_memoise(107, [10, 6, 1])
12
```

## 4. Algorithme *bottom-up* ou ascendant

Nous avions calculé le $f_n$, n-ième terme de la suite de Fibonacci en calculant d'abord $f_0$, $f_1$, $f_2$, ..., jusqu'à $f_{n-1}$ puis $f_n$.

En s'inspirant de cette méthode (*bottom-up* ou méthode ascendante) nous allons ici calculer successivement tous les rendus minimaux jusqu'à ```somme``` avant de calculer le rendu minimal de ```somme```.

!!! example "Exercice 4"
    === "Énoncé"
        Compléter la fonction ```rendu_ascendant``` qui prend en paramètre la somme à rendre ```somme``` et une liste de pièces ```pieces``` et qui renvoie **le nombre minimal** de pièces qu'il faut rendre.

        Nous stockerons chaque rendu dans un dictionnaire ```rendu```, initialisé à la valeur 0 pour la clé 0.

        ```python linenums='1'
        def rendu_ascendant(somme: int, pieces: list) -> int:
            rendu = {...}
            for s in range(..., ...): # (1)
                rendu[s] = ... #(2)
                for piece in pieces:
                    if piece <= s:
                        rendu[s] = min(..., ... + ...)
            return ...   
        ```
        
        1. Attention, il faut aller jusqu'à la valeur ```somme```. 
        2. nombre de pièces dans le pire des cas: on peut faire la somme s avec s pièces.



```python
>>> rendu_ascendant(12, [10, 6, 1])
2
>>> rendu_ascendant(107, [10, 6, 1])
12
```

Notre algorithme itératif est de complexité linéaire (par rapport à la variable ```somme```).

## 5. Bonus : construction d'une solution

Nos différents algorithmes avaient pour but de nous renvoyer le nombre minimal de pièces. Mais peut-on les modifier pour qu'ils renvoient la liste de pièces utilisées ?

Nous allons nous appuyer sur le dernier algorithme créé (par la méthode *bottom-up*).

Il suffit de rajouter un dictionnaire ``solutions`` qui associera à chaque somme la liste des pièces nécessaires.

Lors du parcours de toutes les pièces, si un nouveau nombre minimal de pièces est trouvé pour la pièce ```piece```, il faut rajouter cette pièce au dictionnaire des solutions.

!!! example "Exercice 5"
    === "Énoncé"
        Compléter la fonction ```rendu_solution``` qui prend en paramètre la somme à rendre ```somme``` et une liste de pièces ```pieces``` et qui renvoie la liste des pièces qu'il faut rendre.

        ```python linenums='1'
        def rendu_solution(somme: int, pieces: list) -> list:
            rendu = {0: 0}
            solutions = {}
            solutions[0] = []
            for s in range(1, somme+1):
                rendu[s] = s
                solutions[s] = []
                for piece in pieces:
                    if piece <= s:
                        if 1 + rendu[s-piece] < rendu[s]:
                            rendu[s] = ...
                            solutions[s] = ... .copy() # (1)
                            solutions[s]. ...
            return ...
        ```

        1. on effectue une copie de liste avec la méthode ```copy```. 

```python
>>> rendu_solution(12, [10, 6, 1])
[6, 6]
>>> rendu_solution(107, [1, 6, 10])
[10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 6, 1]
```