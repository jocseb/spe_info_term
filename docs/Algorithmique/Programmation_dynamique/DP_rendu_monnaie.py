# Différentes implémentations du rendu de monnaie

def rendu_glouton(somme: int, pieces: list) -> int:
    """combien de pièces faut-il pour obtenir la somme ?
    Le tableau pieces contient les valeurs des pièces à disposition dans l'ordre croissant."""
    i =  len(pieces) - 1 # position de la première pièce à tester (la plus grande)
    nb_pieces = 0
    while somme > 0 and i < len(pieces): # tant qu'il reste de l'argent à rendre et que toutes les pièces n'ont pas été testées
        valeur = pieces[i] # on prend la pièce d'indice i
        if valeur <= somme: # s'il est possible de rendre la pièce
            nb_pieces = nb_pieces + 1          
            somme = somme - valeur # et on enlève sa valeur de la somme à rendre
        else:
            i = i - 1 # sinon on passe à la pièce immédiatement inférieure 
    return nb_pieces

print(rendu_glouton(12, [1, 6, 10])) # renvoie 3

########################################################################################
def rendu_glouton_v2(somme: int, pieces: list) -> (int, list):
    """combien de pièces faut-il pour obtenir la somme ? Renvoie aussi la liste des pièces rendues.
    Le tableau pieces contient les valeurs des pièces à disposition dans l'ordre croissant."""
    rendu = []
    i =  len(pieces) - 1 # position de la première pièce à tester (la plus grande)
    nb_pieces = 0
    while somme > 0 and i < len(pieces): # tant qu'il reste de l'argent à rendre et que toutes les pièces n'ont pas été testées
        valeur = pieces[i] # on prend la pièce d'indice i
        if valeur <= somme: # s'il est possible de rendre la pièce
            nb_pieces = nb_pieces + 1
            rendu.append(valeur) # on l'ajoute au rendu
            somme = somme - valeur # et on enlève sa valeur de la somme à rendre
        else:
            i = i - 1 # sinon on passe à la pièce immédiatement inférieure 
    return nb_pieces, rendu

print(rendu_glouton_v2(12, [1, 6, 10])) # renvoie (3, [10, 1, 1]), donc solution non optimale car il est possible de faire 12 = 6 + 6 et donc ne rendre que 2 pièces.
########################################################################################

########################################################################################
def rendu_monnaie_glouton(somme: int, pieces: list) -> (int, list):
    """renvoie la solution gloutonne du rendu de monnaie de la somme à rendre entière et positive. 
    Le tableau pieces contient les valeurs des pièces à disposition dans l'ordre décroissant."""
    rendu = []
    i = 0 # position de la première pièce à tester (la plus grande)
    nb_pieces = 0
    while somme > 0 and i < len(pieces): # tant qu'il reste de l'argent à rendre et que toutes les pièces n'ont pas été testées
        valeur = pieces[i] # on prend la pièce d'indice i
        if valeur <= somme: # s'il est possible de rendre la pièce
            nb_pieces = nb_pieces + 1 
            rendu.append(valeur) # on l'ajoute au rendu
            somme = somme - valeur # et on enlève sa valeur de la somme à rendre
        else:
            i = i + 1 # sinon on passe à la pièce immédiatement inférieure
    return nb_pieces, rendu       

# Test
# euros = [500, 200, 100, 50, 20, 10, 5, 2, 1]
# print(rendu_monnaie_glouton(147, euros))
print(rendu_monnaie_glouton(12, [10, 6, 1])) # renvoie (3, [10, 1, 1])
########################################################################################

########################################################################################
# avec un parcours des pièces par valeur, le code est plus simple
def rendre_monnaie_glouton(somme: int, pieces: list) -> (int, list):
    """renvoie la solution gloutonne du rendu de monnaie de la somme à rendre entière et positive. 
    Le tableau pieces contient les valeurs des pièces à disposition dans l'ordre décroissant."""
    rendu = []
    nb_pieces = 0
    for piece in pieces:
        while piece <= somme:
            nb_pieces = nb_pieces + 1 
            rendu.append(piece)
            somme = somme - piece
    return nb_pieces, rendu   

# euros = [500, 200, 100, 50, 20, 10, 5, 2, 1]
# print(rendre_monnaie_glouton(147, euros))
print(rendre_monnaie_glouton(12, [10, 6, 1])) # renvoie (3, [10, 1, 1])
########################################################################################

##############################################################################
def minimum(a, b):
    if a < b:
        return a
    return b
##############################################################################

##############################################################################
def rendu_recursif(somme: int, pieces: list) -> int:
    nb_pieces = somme # nombre de pièces dans le pire des cas
    if somme == 0: # cas de base
        return 0
    for piece in pieces: # peut-on rendre cette pièce ?
        if piece <= somme:
            nb_pieces = minimum(nb_pieces, 1 + rendu_recursif(somme-piece, pieces))
    return nb_pieces

print(rendu_recursif(12, [10, 6, 1])) # renvoie 2
##############################################################################

##############################################################################
memo_rendu = {}
def rendu_recursif_memoise(somme: int, pieces: list) -> int:
    nb_pieces = somme
    if somme == 0:
        return 0
    if somme in memo_rendu:
        return memo_rendu[somme]
    for piece in pieces:
        if piece <= somme:
            nb_pieces = min(nb_pieces, 1 + rendu_recursif_memoise(somme-piece, pieces))
    memo_rendu[somme] = nb_pieces
    return memo_rendu[somme]  

print(rendu_recursif_memoise(12, [10, 6, 1])) # renvoie 2
##############################################################################

##############################################################################
def rendu_ascendant(somme: int, pieces: list) -> int:
    rendu = {0: 0}
    for s in range(1, somme+1): # attention, il faut aller jusqu'à la valeur somme
        rendu[s] = s # nombre de pièces dans le pire des cas: on peut faire la somme s avec s pièces
        for piece in pieces:
            if piece <= s:
                rendu[s] = min(rendu[s], 1 + rendu[s-piece])
    return rendu[somme]

print(rendu_ascendant(12, [10, 6, 1])) # renvoie 2
##############################################################################

##############################################################################
# rendu ascendant avec un tableau
def rendu_ascendant_tab(somme: int, pieces: list) -> int:
    """renvoie le nombre minimal de pièces pour faire
       la somme avec le système pieces"""
    rendu = [0] * (somme + 1)
    for s in range(1, somme + 1):
        rendu[s] = s # s = 1 + 1 + ... + 1 dans le pire des cas
        for piece in pieces:
            if piece <= s:
                rendu[s] = min(rendu[s], 1 + rendu[s - piece])
    return rendu[s]

print(rendu_ascendant_tab(12, [10, 6, 1])) # renvoie 2
##############################################################################

##############################################################################
def rendu_solution(somme: int, pieces: list) -> list:
    rendu = {0: 0}
    solution = {}
    solution[0] = []
    for s in range(1, somme+1):
        rendu[s] = s
        solution[s] = []
        for piece in pieces:
            if piece <= s:
                if 1 + rendu[s-piece] < rendu[s]:
                    rendu[s] = 1 + rendu[s-piece]
                    solution[s] = solution[s-piece].copy()
                    solution[s].append(piece)
    return solution[somme]

print(rendu_solution(12, [10, 6, 1])) # renvoie [6, 6]
print(rendu_solution(107, [1,6,10])) # renvoie [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 6, 1]