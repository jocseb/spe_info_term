# Diviser pour régner

## 1. Retour sur l'algorithme de recherche dichotomique

Nous avons vu en classe de première l'algorithme de **recherche dichotomique** (du grec *dikhotomia*, « division en deux parties »).

Notre but ici est la recherche de la présence (ou non) d'un élément dans un tableau **trié**.  
La fonction qui suit permet de renvoyer la position dans le tableau de la ``valeur_cherchee`` si elle s'y trouve. Elle renvoie `None` sinon.

La recherche *naïve* (élément par élément) est naturellement de complexité linéaire. La méthode dichotomique est plus efficace.

### 1.1. Version impérative

!!! note "Dichotomie version impérative"
    ```python linenums='1'
    def recherche_dichotomique(tableau: list, valeur_cherchee: int):
    """renvoie l'indice de la valeur_cherchee dans le tableau, déjà trié,
    et None si valeur_cherchee ne s'y trouve pas."""
        indice_debut = 0
        indice_fin = len(tableau) - 1
        while indice_debut <= indice_fin:
            indice_central = (indice_debut + indice_fin) // 2   # (1)
            valeur_centrale = tableau[indice_central]           # (2)
            if valeur_centrale == valeur_cherchee:              # (3)
                return indice_central
            if valeur_centrale < valeur_cherchee:               # (4)
                indice_debut = indice_central + 1               # (5)
            else:
                indice_fin = indice_central - 1
        # la valeur_cherchee ne se trouve pas dans tableau
        return None
    ```

    1. on calcule l'indice central avec la division entière
    2. on détermine la valeur centrale
    3. si la valeur centrale est la valeur cherchée...
    4. si la valeur centrale est trop petite...
    5. on ne prend pas la valeur centrale qui a déjà été testée

Exemple d'utilisation :

```python
>>> tab = [1, 2, 2, 5, 6, 6, 7, 9, 9, 10, 10, 13, 13, 15]
>>> recherche_dichotomique(tab, 9), recherche_dichotomique(tab, 20)
(8, None)
```

À chaque tour de la boucle ```while```, la taille du tableau est divisée par 2. Ceci confère à cet algorithme une **complexité logarithmique** (bien meilleure qu'une complexité linéaire).

### 1.2. Version récursive

#### 1.2.1. Dichotomie récursive

Il est possible de programmer de manière récursive la recherche dichotomique sans toucher à la liste, et donc en jouant uniquement sur les indices :

!!! note "Dichotomie version récursive"
    ```python linenums='1'
    def dichotomie_rec(t: list, v: int, g: int, d: int):
    """renvoie une position de v dans t[g..d], déjà trié,
    ou None si v ne s'y trouve pas"""
    if g > d:
        return None
    m = (g + d) // 2
    if v < t[m]:
        return dichotomie_rec(t, v, g, m - 1)
    elif v > t[m]:
        return dichotomie_rec(t, v, m + 1, d)
    else:
        return m
        
    # pour lancer le premier appel sur tout le tableau
    def recherche_dichotomique(t: list, v: int):
        """renvoie une position de v dans le tableau t, ou None si v ne s'y trouve pas"""
        return recherche(t, v, 0, len(t) - 1)
    ```

!!! note "Dichotomie version récursive 2"
    ```python linenums='1'
    def dichotomie_rec_2(tab, val, i=0, j=None): # (1)
        if j is None:                            # (2)
            j = len(tab) - 1
        if i > j:
            return False
        m = (i + j) // 2
        if tab[m] < val:
            return dichotomie_rec_2(tab, val, m + 1, j)
        elif tab[m] > val:
            return dichotomie_rec_2(tab, val, i, m - 1)
        else:
            return True
    ```

    1. Pour pouvoir appeler simplement la fonction sans avoir à préciser les valeurs des indices i et j, on leur donne des paramètres par défaut.
    2. Il est impossible de donner ```j = len(tab)-1``` par défaut (car ```tab``` est aussi un paramètre). On passe donc par une autre valeur (ici ```None```) qu'on va ici intercepter.


Exemple d'utilisation :

```python
>>> tab = [1, 5, 7, 9, 12, 13]
>>> dichotomie_rec_2(tab, 12)
True
>>> dichotomie_rec_2(tab, 17)
False
```

## 2. Diviser pour régner

Les algorithmes de dichotomie présentés ci-dessus ont tous en commun de diviser par deux la taille des données de travail à chaque étape. Cette méthode de résolution d'un problème est connue sous le nom de *diviser pour régner*, ou *divide and conquer* en anglais.  

<!-- Une définition pourrait être : -->

<!-- !!! abstract "Définition"
    Un problème peut se résoudre en employant le paradigme *diviser pour régner* lorsque :  

    - il est possible de décomposer ce problème en sous-problèmes **indépendants** ; 
    - la taille de ces sous-problèmes est une **fraction** du problème initial. -->

!!! gear "Principe"
    La méthode **diviser pour régner** (en anglais *divide and conquer*) est une technique algorithmique de résolution d'un problème qui consiste à :

    1. décomposer le problème initial en (un ou) plusieurs sous-problèmes de taille **inférieure** et **indépendants**;
    2. résoudre chacun des sous-problèmes, éventuellement en les décomposant à leur tour **récursivement** en problèmes plus petits encore ;
    3. combiner (éventuellement) les solutions des sous-problèmes pour obtenir la solution au problème initial (voir plus loin le tri fusion).

    La plupart du temps la résolution des sous-problèmes se fait de façon **récursive**.

**Remarque :**

Considérons l'écriture récursive de la fonction ```factorielle``` ci-dessous :

```python
def factorielle(n):
    if n == 0:
        return 1
    else:
        return n * factorielle(n-1)
```

On ne peut pas parler ici de *diviser pour régner* car la taille des données à traiter est passée de $n$ à $n-1$. C'est bien une diminution (qui fait que l'algorithme fonctionne) mais il n'y a pas de **division** de la taille des données.  
C'est cette division (par 2 dans le cas de la dichotomie) qui donne son efficacité à ce paradigme.

## 3. L'exponentiation rapide

On appelle *exponentiation* le fait de mettre en puissance un nombre. On va donc programmer, de deux manières différentes, la puissance d'un nombre.

### 3.1. Algorithme classique

!!! note "Exponentiation classique"
    ```python linenums='1'
    def puissance(a, n):
        if n == 0:
            return 1
        else:
            return a * puissance(a, n-1)
    ```

### 3.2. Algorithme utilisant *diviser pour régner*

Nous allons nous appuyer sur la remarque mathématique suivante :  

Pour tout nombre $a$,

- si $n$ est pair, $a^n = (a^2)^{\frac{n}{2}}$

- si $n$ est impair, $a^n = a \times a^{n-1} = a \times (a^2)^{\frac{n-1}{2}}$

Ainsi, dans le cas où $n$ est pair, il suffit d'élever $a$ au carré (une seule opération) pour que l'exposant diminue de **moitié**. On peut donc programmer la fonction ```puissance_rapide``` en utilisant le paradigme *diviser pour régner* :

!!! note "Exponentiation rapide"
    ```python linenums='1'
    def puissance_rapide(a, n):
        if n == 0:
            return 1
        if n % 2 == 0:
            return puissance_rapide(a*a, n//2)
        else:
            return a * puissance_rapide(a*a, (n-1)//2)
    ```

### 3.3. Comparaison de la vitesse d'exécution des deux algorithmes

<figure markdown>
![image](data/puiss.png)
<figcaption></figcaption>
</figure>

!!! example "Exercice"
    === "Énoncé"
        Recréer le graphique ci-dessus, qui compare les temps d'exécution des deux fonctions ```puissance``` et ```puissance_rapide```.

        **Aide pour Matplotlib :** le code ci-dessous

        ```python linenums='1'
        import matplotlib.pyplot as plt

        def carre(x):
            return x*x

        x = list(range(10))
        y = [carre(k) for k in x]
        plt.plot(x, y)
        plt.show()
        ```

        donne le graphique suivant :
        <figure markdown>
        ![image](data/carre.png){width=50%}
        <figcaption></figcaption>
        </figure>

<!-- === "Correction"
        ```python linenums='1'
        import matplotlib.pyplot as plt
        import time

        def puissance(a, n):
            if n == 0:
                return 1
            else:
                return a * puissance(a, n-1)

        def puissance_rapide(a, n):
            if n == 0:
                return 1
            if n % 2 == 0:
                return puissance_rapide(a*a, n//2)
            else:
                return a * puissance_rapide(a*a, (n-1)//2)

        def mesure_puissance(n):
            t0 = time.time()
            p = puissance(3, n)
            return time.time() - t0

        def mesure_puissance_rapide(n):
            t0 = time.time()
            p = puissance_rapide(3, n)
            return time.time() - t0
        ``` -->

## 4. Le tri fusion

### 4.1. Principe

L'algorithme du **tri fusion** d'un tableau consiste à :

- partager le tableau en deux moitiés (à une unité près) (diviser) ;
- trier chacune des deux moitiés (régner);
- les fusionner pour obtenir le tableau trié.

On a schématisé le tri de la liste `[10, 6, 3, 9, 7, 5]` suivant ce principe ci-dessous :

```mermaid
    graph TD
    subgraph Diviser en deux
    S["[10, 6, 3, 9, 7, 5]"] --> S1["[10, 6, 3]"]
    S --> S2["[9, 7, 5]"]
    end
    subgraph Fusionner
    S1 -.Trier.-> T1["[3, 6, 10]"]
    S2 -.Trier.-> T2["[5, 7, 9]"]
    T1 --> T["[3, 5, 6, 7, 9, 10]"]
    T2 --> T
    end
```

!!! example "Exercice 1"
        **Question 1**: Le tri des deux moitiés est lui-même effectué par tri fusion, par conséquent que peut-on dire de cet algorithme ?

        Réponse :


!!! example "Exercice 2"
        **Question 2**: On a schématisé ci-dessous le fonctionnement complet de l'algorithme pour la liste `[10, 6, 3, 9, 7, 5]`, compléter les cases manquantes.

        ```mermaid
            graph TD
                    subgraph Diviser en deux
                    S["[10, 6, 3, 9, 7, 5]"] --> S1["[10, 6, 3]"]
                    S --> S2["[9, 7, 5]"]
                    S1 --> S11["[10]"]
                    S1 --> S12["[6, 3]"]
                    S2 --> S21["[9]"]
                    S2 --> S22["[..., ...]"]
                    S12 --> S121["[6]"]
                    S12 --> S122["[3]"]
                    S22 --> S221["[...]"]
                    S22 --> S222["[...]"]
                    end
                    subgraph Fusionner
                    S121 --> T21["[..., ...]"]
                    S122 --> T21
                    S221 --> T22["[5, 7]"]
                    S222 --> T22["[5, 7]"]
                    S11 --> T1["[..., ..., ...]"]
                    T21 --> T1
                    S21 --> T2["[..., ..., ...]"]
                    T22 --> T2
                    T1 --> T["[3, 5, 6, 7, 9, 10]"]
                    T2 --> T
                    end

        ```

!!! example "Exercice 3"
        **Question 3**: Implémentation en Python
        
        Programmer une fonction `diviser(tableau)` qui prend en argument un tableau et renvoie les deux moitiés ``tableau1`` et ``tableau2`` (à une unité près) de `tableau`. Par exemple `diviser([3, 7, 5])` renvoie `[3]` et `[7, 5]`.

        !!! aide
            * Les *slices* de Python sont un moyen efficace d'effectuer le partage, mais leur connaissance n'est pas un attendu du programme de terminale. Les élèves intéressés pourront faire leur propre recherche sur le *Web*.

        ```python
                def diviser(tableau: list) -> (list, list):
                    """ prend en paramètre un tableau et renvoie un tuple de deux tableaux
                    obtenus en "divisant" le tableau en paramètre en deux sous-tableaux de tailles
                    presque égales. """
                    n = len(tableau)
                    moitie = n // 2
                    tableau1, tableau2 = [], []
                    for i in range(n):
                        if i < moitie:
                            tableau1.append(tableau[i])
                        else:
                            tableau2.append(tableau[i])
                    return (tableau1, tableau2)
        ```

!!! example "Exercice 4"
        **Question 4**: Le mécanisme principal du tri fusion est la **fusion** de deux tableaux triés en un nouveau tableau lui aussi trié.
        On donne ci-dessous une fonction `fusionner(tab1, tab2)` qui prend en argument deux tableaux **déjà triés** `tab1` et `tab2` et renvoie le tableau trié `fusion`, fusion de `tab1` et `tab2`.


        **Principe de la fusion de deux tableaux ```tableau1``` et ```tableau2```**

        - on part d'un tableau vide ```fusion```
        - on y ajoute alternativement les éléments de ```tableau1``` et ```tableau2```. Il faut pour cela gérer séparément un indice ```i1``` pour le tableau ```tableau1```  et un indice ```i2```  pour le tableau ```tableau2```.
        - quand un tableau est épuisé, on y ajoute la totalité restante de l'autre tableau.


        ```python linenums="1"
        def fusionner(tab1: list, tab2: list) -> list:
            """ fusionne 2 tableaux triés, pas forcément de même longueur"""
            fusion = [] # tableau trié de la fusion des 2 tableaux en entrée
            n1, n2 = len(tab1), len(tab2)
            i1, i2 = 0, 0 
            while (i1 < n1 and i2 < n2):
                if tab1[i1] <= tab2[i2]:
                    fusion.append(tab1[i1])
                    i1 = i1 + 1
                else:
                    fusion.append(tab2[i2])
                    i2 = i2 + 1
            while i1 < n1: 
                fusion.append(tab1[i1])
                i1 = i1 + 1
            while i2 < n2: 
                fusion.append(tab2[i2])
                i2 = i2 + 1
        return fusion
        ``` 

        1. Recopier et tester cette fonction.
        2. Quel est le rôle des variables `i1` et `i2` ?
        3. Ajouter un commentaire décrivant le rôle de la 1ère boucle `while`.
        4. Ajouter un commentaire décrivant le rôle des lignes 13 à 19.
   
**CORRECTION**

```python linenums="1"
def fusionner(tab1: list, tab2: list) -> list:
    """ fusionne 2 tableaux triés, pas forcément de même longueur"""
    fusion = [] # tableau trié de la fusion des 2 tableaux en entrée
    n1, n2 = len(tab1), len(tab2)
    i1, i2 = 0, 0 # indices pour parcours de tab1 et de tab 2
    while (i1 < n1 and i2 < n2):
        if tab1[i1] <= tab2[i2]:
            fusion.append(tab1[i1])
            i1 = i1 + 1
        else:
            fusion.append(tab2[i2])
            i2 = i2 + 1
    while i1 < n1: # on a fini de parcourir le tableau 2
        fusion.append(tab1[i1])
        i1 = i1 + 1
    while i2 < n2: # on a fini de parcourir le tableau 1
        fusion.append(tab2[i2])
        i2 = i2 + 1
return fusion
```  

**CORRECTION_2**

```python linenums="1"
def fusion(l1,l2):
    ind1=0
    ind2=0
    l = []
    while ind1<len(l1) and ind2<len(l2):
        if l1[ind1]<l2[ind2]:
            l.append(l1[ind1])
            ind1+=1
        else:
            l.append(l2[ind2])
            ind2+=1
    if ind1==len(l1):
        for k in range(ind2,len(l2)):
            l.append(l2[k])
    else:
        for k in range(ind1,len(l1)):
            l.append(l1[k])
    return l
```


        
!!! example "Exercice 5"
        **Question 5**: En utilisant les deux fonctions précédentes, écrire une fonction `tri_fusion(tableau)` qui implémente l'algorithme du tri fusion en python.

        **Principe de l'algorithme du tri fusion**

        - pour trier un tableau, on fusionne les deux moitiés de ce tableau, précédémment eux-mêmes triés par le tri fusion.
        - si un tableau à trier est vide ou réduit à un élément, il est déjà trié.

        ```python
        def tri_fusion(tableau: list) -> list:
            """ implémente l'algo du tri fusion
                entrée : tableau
                sortie : copie triée de tableau"""
            if len(tableau) <= 1: # un tableau vide ou d'1 seul élément est déjà trié
                return tableau
            else:
                tableau1, tableau2 = diviser(tableau)
                tableau1 = tri_fusion(tableau1)
                tableau2 = tri_fusion(tableau2)
                return fusionner(tableau1, tableau2)
        ```

!!! Important
    On montre que l'algorithme du tri fusion a une complexité en $O(n\log(n))$, c'est donc un algorithme plus efficace que le tri par insertion ou le tri par sélection qui ont tous les deux une complexité en $O(n^2)$.

<!-- ## 4. Le tri fusion

### 4.1. Principe

!!! gear "Algorithme du tri fusion (*merge sort*)"
    === "Principe"
        Il s’agit d’un tri suivant le paradigme *diviser pour régner*.  
        Le principe du tri fusion (ou tri par interclassement) est le suivant :

        1. couper le tableau à trier en deux moitiés (diviser) ;
        2. trier indépendammment en place chaque moitié (régner) ;
        3. fusionner (*to merge*) les deux moitiés triées pour reconstituer le tableau trié.


### 4.2. La fusion ou l'interclassement

Le mécanisme principal du tri fusion est la **fusion** de deux listes triées en une nouvelle liste elle aussi triée.

On appelle aussi ce mécanisme : l'**interclassement**.

#### 4.2.1. Principe de la fusion de deux listes ```liste1``` et ```liste2```

- on part d'une liste vide ```liste_fusion```
- on y ajoute alternativement les éléments de ```liste1``` et ```liste2```. Il faut pour cela gérer séparément un indice ```i``` pour la liste ```liste1```  et un indice ```j```  pour la liste ```liste2```.
- quand une liste est épuisée, on y ajoute la totalité restante de l'autre liste.

#### 4.2.2. Implémentation de la fusion de deux listes

!!! example "Exercice"
    === "Énoncé"
        Programmer la fonction ```fusion```.
        ```python
        def fusion(liste1: list, liste2: list) -> list :
            """
            prend en paramètre deux listes triées par ordre croissant
            et renvoie la liste triée aussi par ordre croissant constituée
            des éléments des deux listes.
            """
            pass
        ```
    <!-- === "Correction"
        ```python
        def fusion(liste1: list, liste2: list) -> list :
            """
            prend en paramètre deux listes triées par ordre croissant
            et renvoie la liste triée aussi par ordre croissant constituée
            des éléments des deux listes.
            """
            liste_fusion = []
            i, j = 0, 0
            while i < len(liste1) and j < len(liste2):
                if liste1[i] < liste2[j]:
                    liste_fusion.append(liste1[i])
                    i += 1
                else:
                    liste_fusion.append(liste2[j])
                    j += 1
            liste_fusion = liste_fusion + liste1[i:] + liste2[j:] # On ajoute les éléments restants
            return liste_fusion
        ``` -->
<!--
    === "Correction"
        ```python
        def fusionner(tab1: list, tab2: list) -> list:
            """ fusionne 2 tableaux triés, pas forcément de même longueur"""
            fusion = [] # tableau trié de la fusion des 2 tableaux en entrée
            n1, n2 = len(tab1), len(tab2)
            i1, i2 = 0, 0 # indices pour parcours de tab1 et de tab 2
            while (i1 < n1 and i2 < n2):
                if tab1[i1] <= tab2[i2]:
                    fusion.append(tab1[i1])
                    i1 = i1 + 1
                else:
                    fusion.append(tab2[i2])
                    i2 = i2 + 1
            while i1 < n1: # on a fini de parcourir le tableau 2
                fusion.append(tab1[i1])
                i1 = i1 + 1
            while i2 < n2: # on a fini de parcourir le tableau 1
                fusion.append(tab2[i2])
                i2 = i2 + 1
        return fusion
        ``` 


### 4.3. La découpe

#### 4.3.1. Principe de l'algorithme de découpe

L'idée du tri fusion est le découpage de la liste originale en une multitude de listes ne contenant qu'un seul élément. Ces listes élémentaires seront ensuite fusionnées (interclassées) avec la fonction ```fusion``` écrite précédente.

<figure markdown>
![image](data/dessin_tri_fusion_decoupe_fusion.png)
<figcaption></figcaption>
</figure>

<!-- <figure markdown>
![image](data/fusion.png)
<figcaption></figcaption>
</figure> -->

<!--
#### 4.3.2. Implémentation de l'algorithme de découpe

La fonction ``decoupe`` sépare les éléments d'une liste en 2 listes de même taille, à un près, qui sont renvoyées sous la forme d'un couple de listes (tuple). Dans la 1ère liste, on met les $n/2$ 1ers éléments et le reste dans la seconde liste.

!!! example "Exercice"
    === "Énoncé"
        Programmer la fonction ```decoupe```.
        ```python
        def decoupe(liste: list) -> (list, list):
            """
            prend en paramètre une liste et renvoie deux listes
            obtenues en "découpant" la liste en paramètre en deux sous-listes de tailles
            presque égales.
            """
        pass
        ```
    <!-- === "Correction"
        ```python
        def decoupe(liste: list) -> (list, list):
            """
            prend en paramètre une liste et renvoie deux listes
            obtenues en "découpant" la liste en paramètre en deux sous-listes de tailles
            presque égales.
            """
            moitie = len(liste) // 2
            liste1, liste2 = [], []
            for i in range(len(liste)):
                if i < moitie:
                    liste1.append(liste[i])
                else:
                    liste2.append(liste[i])
            return (liste1, liste2)
        ``` -->
<!--
    === "Correction"
        ```python
        def diviser(tableau: list) -> (list, list):
            """ prend en paramètre un tableau et renvoie deux tableaux
            obtenus en "divisant" le tableau en paramètre en deux sous-tableaux de tailles
            presque égales. """
            n = len(tableau)
            moitie = n // 2
            tableau1, tableau2 = [], []
            for i in range(n):
                if i < moitie:
                    tableau1.append(tableau[i])
                else:
                    tableau2.append(tableau[i])
            return (tableau1, tableau2)
        ```

### 4.4. Le tri fusion

#### 4.4.1. Principe de l'algorithme du tri fusion

- pour trier une liste, on interclasse (fusionne) les deux moitiés de cette liste, précédémment elles-mêmes triées par le tri fusion.
- si une liste à trier est réduite à un élément, elle est déjà triée.

#### 4.4.2. Implémentation de l'algorithme du tri fusion

La grande force de ce tri va être qu'il se programme simplement de manière **récursive**, en appelant à chaque étape la même fonction mais avec une taille de liste divisée par deux, ce qui justifie son classement parmi les algorithmes utilisants «diviser pour régner».

!!! example "Exercice : tri fusion (*merge sort*)"
    Compléter la fonction ```tri_fusion```.
    ```python
    def tri_fusion(tab: list) -> list:
    "Fonction qui effectue le tri fusion sur une liste d'entiers."
    if len(tab) <= 1:
        return ...
    else:
        tab1, tab2 = ...
        return fusion(..., ...)
    ```

<!-- !!! abstract "Algorithme de tri fusion (*merge sort*)"
    ```python
    def tri_fusion(tab: list) -> list:
    "Fonction qui effectue le tri fusion sur une liste d'entiers."
    if len(tab) <= 1:
        return tab
    else:
        tab1, tab2 = decoupe(tab)
        return fusion(tri_fusion(tab1),tri_fusion(tab2))
    ``` -->

<!--  
    <!-- ```python
    def interclassement(lst1, lst2):
        lst_totale = []
        n1, n2 = len(lst1), len(lst2)
        i1, i2 = 0, 0
        while i1 < n1 and i2 < n2:
            if lst1[i1] < lst2[i2]:
                lst_totale.append(lst1[i1])
                i1 += 1
            else:
                lst_totale.append(lst2[i2])
                i2 += 1
        return lst_totale + lst1[i1:] + lst2[i2:]

    def tri_fusion(lst):
        if len(lst) <= 1:
            return lst
        else:
            m = len(lst) // 2
            return interclassement(tri_fusion(lst[:m]), tri_fusion(lst[m:]))
    ```-->
<!--
    === "Correction"
        ```python
        def tri_fusion(tableau: list) -> list:
            """ implémente l'algo du tri fusion
                entrée : tableau
                sortie : copie triée de tableau"""
            if len(tableau) <= 1: # un tableau vide ou d'1 seul élément est déjà trié
                return tableau
            else:
                tableau1, tableau2 = diviser(tableau)
                tableau1 = tri_fusion(tableau1)
                tableau2 = tri_fusion(tableau2)
                return fusionner(tableau1, tableau2)
        ```

 -->

### 4.2. Visualisation du tri fusion

Une erreur classique avec les fonctions récursives est de considérer que les appels récursifs sont simultanés. Ceci est faux !
L'animation suivante montre la progression du tri :

<gif-player src="https://jocseb.gitlab.io/spe_info_term/Diviser%20pour%20r%C3%A9gner/data/gif_fusion.gif" speed="1" play></gif-player>

_On peut contrôler le déroulement de l'animation en la survolant de gauche à droite avec la souris._

<!-- Il est aussi conseillé d'observer l'évolution de l'algorithme grâce à PythonTutor :

 -->

### 4.3. Complexité du tri fusion

La division par 2 de la taille du tableau pourrait nous amener à penser que le tri fusion est de complexité logarithmique, comme l'algorithme de dichotomie. Il n'en est rien.

En effet, l'instruction finale ```fusionner(tri_fusion(tableau1),tri_fusion(tableau2))``` lance **deux** appels à la fonction ```tri_fusion``` (avec des données d'entrée deux fois plus petites).

On peut montrer que :

!!! note "Complexité du tri fusion"
    L'algorithme de tri fusion est en $O(n \log n)$.

    On dit qu'il est **quasi-linéaire**.

C'est donc un algorithme plus efficace que le tri par insertion ou le tri par sélection qui ont tous les deux une complexité en $O(n^2)$.

Une complexité quasi-linéaire (en $O(n \log n)$) se situe «entre» une complexité linéaire (en $O(n)$) et une complexité quadratique (en $O(n^2)$). Mais elle est plus proche de la complexité linéaire :

<figure markdown>
![image](data/comparaison.png)
<figcaption></figcaption>
</figure>

Une jolie animation permettant de comparer les implémentations de différents algorithmes de tri :

<figure markdown>
![image](data/toptalcom_sorting_algorithms.gif)
<figcaption></figcaption>
</figure>

Issue de ce [site](https://www.toptal.com/developers/sorting-algorithms){:target="_blank"}
