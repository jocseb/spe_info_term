import sys 
import timeit
import matplotlib.pyplot as plt

sys.setrecursionlimit(2*10**4) # Pour modifier le nombre maximal d'appels récursifs

abscisse = [100, 200, 300, 400, 500, 600]
x = 2
ordonnee = []
for n in abscisse:
    temps=timeit.timeit("exponentiation(x, n)", number=100, globals=globals())
    ordonnee.append(temps)

plt.plot(abscisse, ordonnee, "ro-", label="Récursif Classique")

plt.xlabel("Exposant")
plt.ylabel("Temps en s")

plt.legend()
plt.show()
plt.close()