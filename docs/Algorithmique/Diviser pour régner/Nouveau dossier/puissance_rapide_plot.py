import matplotlib.pyplot as plt
import time

def puissance(a, n):
    if n == 0:
        return 1
    else:
        return a * puissance(a, n-1)

def puissance_rapide(a, n):
    if n == 0:
        return 1
    if n % 2 == 0:
        return puissance_rapide(a*a, n//2)
    else:
        return a * puissance_rapide(a*a, (n-1)//2)

def mesure_puissance(n):
    t0 = time.time()
    p = puissance(3, n)
    return time.time() - t0

def mesure_puissance_rapide(n):
    t0 = time.time()
    p = puissance_rapide(3, n)
    return time.time() - t0


def carre(x):
    return x*x

# x = list(range(10))
# y = [carre(k) for k in x]
# plt.plot(x, y)
# plt.show()

x = list(range(200))
# y = [mesure_puissance(k) for k in x]
y = [mesure_puissance_rapide(k) for k in x]
plt.plot(x, y)
plt.show()
